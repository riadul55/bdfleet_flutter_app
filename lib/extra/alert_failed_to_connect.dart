import 'package:flutter/material.dart';

class AlertFailedToConnect{
  BuildContext _buildContext;
  AlertFailedToConnect(this._buildContext);
  show(msg,{VoidCallback retry}) {
      AlertDialog alert = AlertDialog(
        content: Text(msg),
        actions: <Widget>[
          retry != null?FlatButton(
            child: Text("Try Again"),
            onPressed: () {
              Navigator.pop(_buildContext);
              retry();
            },
          ):Container(),
          FlatButton(
            child: Text("Ok"),
            onPressed: () {
              Navigator.pop(_buildContext);
            },
          )
        ],
      );
      showDialog(
        context: _buildContext,
        builder: (ctx) {
          return alert;
        },
      );
    }
}