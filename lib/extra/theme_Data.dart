import 'package:bdfleet/extra/hex_color.dart';
import 'package:bdfleet/model/profile.dart';
import 'package:flutter/material.dart';
import 'package:bdfleet/model/device_model_local.dart';
import 'package:latlong/latlong.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:bdfleet/model/alert_model.dart';


Color appsMainColor = HexColor.fromHex("#006776");
Color appsMainColor2 = HexColor.fromHex("#0097A7");
Color appsSubColor = HexColor.fromHex("#F38F1C");
Color appsColorGreen = HexColor.fromHex("#00A550");
Color appsColorGreenDark = HexColor.fromHex("#01974A");

List<DeviceModelLocal> allDevicesData = List<DeviceModelLocal>();
Profile profile;
List<LatLng> polyLatLng = [];
Set<Polygon> polygonList = {};
LatLng currentLocation = LatLng(23.7495101,90.3889223);
MapController controllerDashboard;
List<AlertModel> allAlerts = [];

final loaderMaterial = Center(
  child: CircularProgressIndicator(
    valueColor: AlwaysStoppedAnimation<Color>(appsMainColor),
  ),
);
final loaderMaterialLinear = LinearProgressIndicator(
  valueColor: AlwaysStoppedAnimation<Color>(appsMainColor),
  backgroundColor: Colors.white,
);

