import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProgressLoader{
  static show(){
    showDialog(
      barrierDismissible: false,
      context: Get.context,
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: true,
          content: Column(
            children: [
              Container(
                margin: EdgeInsets.only(bottom: 10),
                child: Text("Loading"),
              ),
              LinearProgressIndicator()
            ],
          )
        );
      },
    );
  }
  static hide(){
    Navigator.pop(Get.context);
  }
}