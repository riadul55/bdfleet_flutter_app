import 'package:bdfleet/extra/theme_Data.dart';
import 'package:flutter/material.dart';

class AlertModel{
  String id,alertTitle,description,user_id,device_id,vehicleNo,location,locationAddress;
  DateTime timestamp;
  int type_id,acknowledged,invoice_id;
  double speed,battery;
  AlertModel({this.location,this.alertTitle,this.timestamp,this.vehicleNo,this.id,this.description,
    this.battery,this.speed,this.type_id,this.acknowledged,this.device_id,this.invoice_id,this.user_id,this.locationAddress});

  factory AlertModel.fromJSON(Map<String,dynamic> data){
    String vNumber = "No info";
    try {
      vNumber = allDevicesData
          .singleWhere((element) => element.id == data["device_id"])
          .description;
    }catch(e){

    }
    return AlertModel(
    id: data["id"],
    timestamp: DateTime.parse(data["timestamp"]+"Z").toLocal(),
    type_id: data["type_id"],
    alertTitle: getAlertTitle(data["type_id"].toString()),
    description: data["description"],
    location: data["location"],
    user_id: data["user_id"],
    device_id: data["device_id"],
    vehicleNo: vNumber,
    acknowledged: data["acknowledged"],
    speed: data["speed"],
    battery: data["battery"],
    invoice_id: data["invoice_id"],
  );
  }
  static getAlertTitle(String id){
    switch(id){
      case '1': return "LOW_BATTERY";
      break;
      case '2': return "POWER_CUT";
      break;
      case '3': return "GEO_FENCE";
      break;
      case '4': return "SPEED";
      break;
      case '5': return "MOVEMENT";
      break;
      case '6': return "TRACKER";
      break;
      case '7': return "ENGINE_ON";
      break;
      case '8': return "ENGINE_OFF";
      break;
      case '9': return "DOOR";
      break;
      case '10': return "SENSOR";
      break;
      case '11': return "ACC";
      break;
      case '12': return "ACCIDENT";
      break;
      case '13': return "BONNET";
      break;
      case '14': return "FOOTBRAKE";
      break;
      case '15': return "TEMPERATURE";
      break;
      case '16': return "LOW FUEL";
      break;
      case '17': return "DIAGNOSTIC_P0001";
      break;
      case '18': return "SERVICE";
      break;
      case '19': return "ALERM_SOS";
      break;
      case '20': return "ALERM_VIBRATION";
      break;
      case '100': return "NO_GPS_SIGNAL";
      break;
      case '101': return "GSM_SIGNAL_ON";
      break;
     // case '102': return "GSM_SIGNAL_OFF";
      default: return "GSM_SIGNAL_OFF";
    }


    }
}