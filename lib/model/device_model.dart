import 'package:bdfleet/model/device_model_local.dart';

class DeviceUpdateModel{
  String vehicleNumber,userId,deviceId,locationName,speed,lat,lon;
  VehicleType type;
  double heading,fuel,temperature,millage;
  String distance,vehicleType;
  int battery,door,gsmSignal,gpsSatellite,gpsSignal,status,online,typeID,lifeCycle;
  RunningStatus runningStatus;
  DateTime timestamp,lastTimestamp;
  EngineStatus engine;
  bool lsb,gpsPositioning;

  DeviceUpdateModel({
      this.vehicleNumber, this.userId, this.deviceId, this.locationName, this.type, this.speed, this.lat,
      this.lon, this.heading,this.millage, this.fuel, this.temperature, this.distance, this.battery, this.door, this.gsmSignal,
      this.gpsSatellite, this.gpsSignal, this.status, this.runningStatus, this.timestamp, this.lastTimestamp,
      this.engine, this.lsb, this.online, this.gpsPositioning,this.lifeCycle,this.typeID,this.vehicleType
  });
  factory DeviceUpdateModel.fromJSON(Map<String,dynamic> data){
    return DeviceUpdateModel(
      battery: data["battery"],
      userId: data["user_id"],
      timestamp: DateTime.parse(data["timestamp"]+'Z'),
      lastTimestamp:data["last_timestamp"]==null?DateTime.now().toUtc():DateTime.parse(data["last_timestamp"]+'Z'),
      lon: data["lon"],
      lat: data["lat"],
      speed: data["speed"],
      heading: data["heading"],
      millage: data["mileage"],
      distance: data["distance"],
      deviceId: data["device_id"],
      locationName: data["location_name"],
      vehicleNumber: data["description"],
      engine: data["engine"]==1?EngineStatus.on:EngineStatus.off,
      door: data["door"],
      online: data["online"],
      runningStatus: getRunningStatus(data["speed"]),
      fuel: data["fuel"],
      temperature: data["temperature"],
      lsb: data["lsb"],
      gsmSignal: data["gsm_signal"],
      gpsSatellite: data["gps_sattelites"],
      gpsPositioning: data["gps_positioning"],
      gpsSignal: data["gps_signal"],
      status: data["status"],
    );
  }
  static getRunningStatus(String value){
    RunningStatus runningStatus = RunningStatus.idle;
    double speed = 0;
    try{
      speed = double.parse(value);
    }catch(e){}
    if(speed > 80){
      runningStatus = RunningStatus.over;
    }else if(speed > 10){
        runningStatus = RunningStatus.running;
    }else if(speed > 0){
      runningStatus = RunningStatus.slow;
    }else {
      runningStatus = RunningStatus.idle;
    }
    return runningStatus;
  }
}