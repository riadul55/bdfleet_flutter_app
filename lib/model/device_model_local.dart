import 'dart:core';

class DeviceModelLocal{
  String id, description,vehicle_type,status,geoFence;
  int	type_id,life_cyle;
  EngineStatus engine;
  RunningStatus runningStatus;
  DateTime timestamp,lastTimestamp;
  String locationName,speed,lat,lon,idleTime;
  int battery,door,gsmSignal,gpsSatellite,gpsSignal,online,typeID,lifeCycle;
  bool lsb;
  VehicleType vehicleType;
  double heading,millage;
  DeviceModelLocal({
    this.id,this.description,this.life_cyle,this.status,this.type_id,this.vehicle_type,this.geoFence,
    this.speed,this.engine,this.millage,this.lastTimestamp,this.lat,this.locationName,this.lon,this.lsb,this.timestamp,
    this.runningStatus,this.vehicleType,this.idleTime,this.heading,this.typeID,this.lifeCycle,this.online,this.battery,this.door,this.gpsSatellite,this.gpsSignal,this.gsmSignal
  });

  factory DeviceModelLocal.fromJSON(Map<String,dynamic> data){
    return DeviceModelLocal(
      id: data["id"],
      description: data["description"],
      vehicle_type: data["vehicle_type"],
      type_id: data["type_id"],
      life_cyle: data["life_cyle"],
      idleTime: "0 Seconds",
      geoFence: data["geofence_DCCarea"],
      status: data["status"],
        runningStatus : RunningStatus.idle,
      engine: EngineStatus.off,
      speed: "0",
      vehicleType: data["vehicle_type"]=="cng"?VehicleType.cng:data["vehicle_type"]=="bike"?VehicleType.bike:data["vehicle_type"]=="truck"?VehicleType.truck:VehicleType.car
    );
  }
  Map<String,dynamic> toJSON(DeviceModelLocal data){
    return {
    "id" :data.id,
    "description" : data.description,
    "vehicle_type" : data.vehicle_type,
    "type_id" : data.type_id,
    "life_cyle": data.life_cyle,
    "status": data.status,
    "geofence_DCCarea": data.geoFence
    };
  }
}
enum EngineStatus {
  on,
  off
}
enum RunningStatus {
  running,
  slow,
  over,
  idle,
}
enum VehicleType {
  bike,
  car,
  truck,
  cng
}