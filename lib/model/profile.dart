class Profile{
  String userId,userName;
  int customerId,typeId,languageId,countryId;
  ProfileInfo profileInfo;
  Profile({this.userId,this.userName,this.customerId,this.countryId,this.languageId,this.typeId,this.profileInfo});

  factory Profile.fromJson(Map<String,dynamic> data){
    return Profile(
      userId: data["user_id"],
      userName: data["username"],
      customerId: data["customer_id"],
      typeId: data["type_id"],
      countryId: data["country_id"],
      languageId: data["language_id"],
      profileInfo: ProfileInfo.fromJSON(data["profile"])
    );
  }
  Map<String,dynamic> toJSON(Profile profile){
    return {
      "user_id": profile.userId,
      "username": profile.userName,
      "type_id": profile.typeId,
      "country_id": profile.countryId,
      "language_id": profile.languageId,
      "customer_id": profile.customerId,
      "profile": ProfileInfo.toJSON(profile.profileInfo),
    };
  }
}

class ProfileInfo{
  String image,mob2,user_status,mob1,address3,address2,city,address1,garage,first_name,last_name,employee_id,geofence_DCCarea;

  ProfileInfo({
      this.image,
      this.mob2,
      this.user_status,
      this.mob1,
      this.address3,
      this.address2,
      this.city,
      this.address1,
      this.garage,
      this.first_name,
      this.last_name,
      this.employee_id,
      this.geofence_DCCarea});
  factory ProfileInfo.fromJSON(Map<String,dynamic> data){
    return ProfileInfo(
      image: data["image"],
      mob2: data["mob2"],
      user_status: data["user_status"],
      mob1: data["mob1"],
      address3: data["address3"],
      address2: data["address2"],
      city: data["city"],
      address1: data["address1"],
      garage: data["garage"],
      first_name: data["first_name"],
      last_name: data["last_name"],
      employee_id: data["employee_id"],
      geofence_DCCarea: data["geofence_DCCarea"],
    );
  }
  static Map<String,dynamic> toJSON(ProfileInfo profileInfo){
    return {
      "image": profileInfo.image,
      "mob2": profileInfo.mob2,
      "user_status": profileInfo.user_status,
      "mob1": profileInfo.mob1,
      "address3": profileInfo.address3,
      "address2": profileInfo.address2,
      "city": profileInfo.city,
      "address1": profileInfo.address1,
      "garage": profileInfo.garage,
      "last_name": profileInfo.last_name,
      "employee_id": profileInfo.employee_id,
      "geofence_DCCarea": profileInfo.geofence_DCCarea,
      "first_name": profileInfo.first_name
    };
  }
}