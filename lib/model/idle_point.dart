import 'package:latlong/latlong.dart';
class IdlePointModel{
  LatLng point;
  int idleTime;
  String location;
  IdlePointModel({this.point,this.location,this.idleTime});
}