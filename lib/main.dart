import 'package:bdfleet/pages/login/login_page.dart';
import 'package:bdfleet/provider/device_update_provider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
      MultiProvider(
        providers: [
          ChangeNotifierProvider(
              create: (context) => DeviceUpdate()
          ),
        ],
        child: MyApp(),
      ),
  );
}
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.teal,
        primarySwatch: Colors.cyan,
      ),
      home: LoginPage(),
    );
  }
}