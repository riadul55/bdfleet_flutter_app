import 'dart:convert';
import 'dart:io';

import 'package:bdfleet/model/device_model.dart';
import 'package:bdfleet/service/client/web_client.dart';
import 'package:bdfleet/service/helper/reponse.dart';
import 'package:bdfleet/storage/shared_pref_manager.dart';
import 'package:geocoder/geocoder.dart';
import 'package:latlong/latlong.dart';

class LocationService{
  Future<ApiResponse> getLocationName(LatLng latLng) async{
    final coordinates = new Coordinates(latLng.latitude, latLng.longitude);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    return ApiResponse(statusCode: 200, message: first.addressLine??"");
  }
  Future<ApiResponse> getLocationHistory(deviceId,String from,String to) async{
    HttpClientResponse response = await WebApiClient.getInstance.get("location/query/device/$deviceId?start=$from&end=$to&snap=1");
    if (response == null) {
      return ApiResponse(statusCode: 500, message: "Failed to connect !");
    }else if(response.statusCode == 200){
      List<DeviceUpdateModel> allLocation = [];
      String body = await response.transform(utf8.decoder).join();
      dynamic res = jsonDecode(body);
      for(var item in res){
        allLocation.add(DeviceUpdateModel.fromJSON(item));
      }
      return ApiResponse(statusCode: 200, message: allLocation);
    }else
      return ApiResponse(statusCode: 404, message: "failed to load profile info");
  }
}