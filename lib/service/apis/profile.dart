import 'dart:convert';
import 'dart:io';
import 'dart:io' as io;

import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/profile.dart';
import 'package:bdfleet/service/client/web_client.dart';
import 'package:bdfleet/service/helper/reponse.dart';
import 'package:bdfleet/storage/shared_pref_manager.dart';

class ProfileService{
  Future<ApiResponse> getProfileInfo() async{
    String userId = await SharedPrefManager.getUserId();
    HttpClientResponse response = await WebApiClient.getInstance.get("access/user/$userId");
    if (response == null) {
      return ApiResponse(statusCode: 500, message: "Failed to connect !");
    }else if(response.statusCode == 200){
      String body = await response.transform(utf8.decoder).join();
      dynamic res = jsonDecode(body);
      Profile profile = Profile.fromJson(res);
      return ApiResponse(statusCode: 200, message: profile);
    }else
      return ApiResponse(statusCode: 404, message: "failed to load profile info");
  }
  Future<ApiResponse> updateProfile(String userName,String firstName,String lastName) async{
    String userId = await SharedPrefManager.getUserId();
    var formData = jsonEncode(
      {
        "user_id": userId,
        "username": userName,
        "type_id": profile.typeId,
        "language_id": profile.languageId,
        "profile": {
          "last_name": lastName,
          "first_name": firstName
        }
    });
    HttpClientResponse response = await WebApiClient.getInstance.put(formData, "access/user");
    if (response == null) {
      return ApiResponse(statusCode: 500, message: "Failed to connect !");
    }else{
      print(response.statusCode);
      if(response.statusCode == 202){
        ApiResponse response = await ProfileService().getProfileInfo();
        if(response.statusCode == 200){
          profile = response.message;
        }
        return ApiResponse(statusCode: 202, message: "Update success");
      }else
        return ApiResponse(statusCode: 404, message: "Failed to update");
    }
  }
  Future<ApiResponse> updatePassword(String userName,String password) async{
    String userId = await SharedPrefManager.getUserId();
    var formData = jsonEncode(
        {
          "user_id": userId,
          "username": userName,
          "password": "Afsar123",
          "type_id": profile.typeId,
          "language_id": profile.languageId,
          "profile": {

          }
        });
    HttpClientResponse response = await WebApiClient.getInstance.put(formData, "access/user");
    if (response == null) {
      return ApiResponse(statusCode: 500, message: "Failed to connect !");
    }else{
      print(response.statusCode);
      if(response.statusCode == 202){
        return ApiResponse(statusCode: 202, message: "Update success");
      }else
        return ApiResponse(statusCode: 404, message: "Failed to update");
    }
  }
  Future<ApiResponse> updateAddress(String firstLine,String secondLine,String thirdLine,String city) async{
    var formData = jsonEncode(
        {
          "user_id": profile.userId,
          "username": profile.userName,
          "type_id": profile.typeId,
          "language_id": profile.languageId,
          "profile": {
            "address1": firstLine,
            "address2": secondLine,
            "address3": thirdLine,
            "city": city,
          }
        });
    HttpClientResponse response = await WebApiClient.getInstance.put(formData, "access/user");
    if (response == null) {
      return ApiResponse(statusCode: 500, message: "Failed to connect !");
    }else{
      print(response.statusCode);
      if(response.statusCode == 202){
        ApiResponse response = await ProfileService().getProfileInfo();
        if(response.statusCode == 200){
          profile = response.message;
        }
        return ApiResponse(statusCode: 202, message: "Update success");
      }else
        return ApiResponse(statusCode: 404, message: "Failed to update");
    }
  }
}