import 'dart:convert';
import 'dart:io';

import 'package:bdfleet/model/device_model_local.dart';
import 'package:bdfleet/service/client/web_client.dart';
import 'package:bdfleet/service/helper/reponse.dart';
import 'package:bdfleet/storage/shared_pref_manager.dart';

class LoginService{
  Future<ApiResponse> loginNow(String userName,String password) async{
    var formData = jsonEncode({
      "username":userName,
      "password":password
    });
    HttpClientResponse response = await WebApiClient.getInstance.post(formData, "access/login");
    if (response == null) {
      return ApiResponse(statusCode: 500, message: "Failed to connect !");
    }else if(response.statusCode == 200){
      String body = await response.transform(utf8.decoder).join();
      dynamic res = jsonDecode(body);
      SharedPrefManager.setUserId(res["user_id"]);
      SharedPrefManager.setToken(res["session_id"]);
      return ApiResponse(statusCode: 200, message: "Login success");
    }else
      return ApiResponse(statusCode: 404, message: "Either email or password didn't match");
  }
  Future<ApiResponse> getAllDeviceList(String userId) async{
    String userID = await SharedPrefManager.getUserId();
    HttpClientResponse response = await WebApiClient.getInstance.get("device/$userID/query");
    if (response == null) {
      return ApiResponse(statusCode: 500, message: "Failed to connect !");
    }else if(response.statusCode == 200){
      String body = await response.transform(utf8.decoder).join();
      dynamic res = jsonDecode(body);
      List<DeviceModelLocal> deviceList = [];
      int i = 1;
      for(var data in res){
        String deviceID = data["id"];
        try{
          HttpClientResponse deviceRes = await WebApiClient.getInstance.get("device/$deviceID");
          if(deviceRes != null){
            if(deviceRes.statusCode == 200){
              await Future.value(
                  deviceRes.cast<List<int>>().transform(utf8.decoder).listen((_body) {
                    dynamic _res = jsonDecode(_body);
                    deviceList.add(DeviceModelLocal(
                      id: deviceID,
                      description: data["description"],
                      life_cyle:  data["lifecycle"],
                      status: _res["properties"]["status"],
                      vehicle_type: _res["properties"]["vehicle_type"],
                      type_id:  data["type_id"],
                      geoFence:  _res["properties"]["geofence_DCCarea"],
                    ));
                  })
              );
            }
          }
        }catch(e){
          print("ERRORORORORO "+e.toString());
        }

      }
      return ApiResponse(statusCode: 200, message: deviceList);
    }else
      return ApiResponse(statusCode: 404, message: "failed to load data, please try again later");
  }

}