import 'dart:convert';
import 'dart:io';
import 'package:bdfleet/model/device_model.dart';
import 'package:bdfleet/service/client/web_client.dart';
import 'package:bdfleet/service/helper/reponse.dart';
import 'package:latlong/latlong.dart';

class WayPointNavigation{
  Future<ApiResponse> getRouteWayPoints(LatLng fromLatLng ,LatLng toLatLng ) async{
    HttpClientResponse response = await WebApiClient.getInstance.get("route/${fromLatLng.latitude},${fromLatLng.longitude}/${toLatLng.latitude},${toLatLng.longitude}");
    if (response == null) {
      return ApiResponse(statusCode: 500, message: "Failed to connect !");
    }else if(response.statusCode == 200){
      String body = await response.transform(utf8.decoder).join();
      var data = jsonDecode(body);
      return ApiResponse(statusCode: 200, message: data["routes"][0]);
    }else
      return ApiResponse(statusCode: 404, message: "failed to load profile info");
  }
}