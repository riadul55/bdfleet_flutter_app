import 'dart:convert';
import 'dart:io';
import 'package:bdfleet/model/device_model.dart';
import 'package:bdfleet/service/client/web_client.dart';
import 'package:bdfleet/service/helper/reponse.dart';
import 'package:bdfleet/storage/shared_pref_manager.dart';

class DeviceUpdateService{
  Future<ApiResponse> getUpdate() async{
    String userId = await SharedPrefManager.getUserId();
    HttpClientResponse response = await WebApiClient.getInstance.get("location/query/user/$userId/last");
    if (response == null) {
      return ApiResponse(statusCode: 500, message: "Failed to connect !");
    }else if(response.statusCode == 200){
      List<DeviceUpdateModel> allUpdates = [];
      String body = await response.transform(utf8.decoder).join();
      dynamic res = jsonDecode(body);
      for(var item in res){
        allUpdates.add(DeviceUpdateModel.fromJSON(item));
      }
      return ApiResponse(statusCode: 200, message: allUpdates);
    }else
      return ApiResponse(statusCode: 404, message: "failed to load profile info");
  }
}