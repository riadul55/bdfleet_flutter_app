import 'dart:convert';
import 'dart:io';

import 'package:bdfleet/model/alert_model.dart';
import 'package:bdfleet/service/apis/location.dart';
import 'package:bdfleet/service/client/web_client.dart';
import 'package:bdfleet/service/helper/reponse.dart';
import 'package:bdfleet/storage/shared_pref_manager.dart';
import 'package:latlong/latlong.dart';
class DeviceAlertService{
  Future<ApiResponse> getAllAlerts(String from,String to) async{
    String userId = await SharedPrefManager.getUserId();
    HttpClientResponse response = await WebApiClient.getInstance.get("alert/user/$userId?start=$from&end=$to");
    print("alert/user/$userId?start=$from&end=$to");
    if (response == null) {
      return ApiResponse(statusCode: 500, message: "Failed to connect !");
    }else if(response.statusCode == 200){
      List<AlertModel> alerts = [];
      String body = await response.transform(utf8.decoder).join();
      dynamic res = jsonDecode(body);
      for(var item in res){
        AlertModel _model = AlertModel.fromJSON(item);
        try{
        if(_model.location !=null){
          List<String> latLng  = _model.location.split(";");
          LatLng _l = LatLng(double.parse(latLng.first),double.parse(latLng.last));
          if(_l.latitude !=0.0 && _l.longitude != 0.0){
            ApiResponse addresses = await LocationService().getLocationName(_l);
            _model.locationAddress = addresses.message;
          }else{
            _model.locationAddress = "";
          }
        }else{
          _model.locationAddress = "";
        }
        }catch(e){
          print("Error "+_model.location);
          _model.locationAddress = "";
        }
        alerts.add(_model);
      }
      return ApiResponse(statusCode: 200, message: alerts);
    }else
      return ApiResponse(statusCode: 404, message: "failed to load");
  }
  Future<ApiResponse> getAlertsSingleDevice(String from,String to,String deviceId) async{
    HttpClientResponse response = await WebApiClient.getInstance.get("alert/device/$deviceId?start=$from&end=$to");
    print("alert/user/$deviceId?start=$from&end=$to");
    if (response == null) {
      return ApiResponse(statusCode: 500, message: "Failed to connect !");
    }else if(response.statusCode == 200){
      List<AlertModel> alerts = [];
      try{
      String body = await response.transform(utf8.decoder).join();
      dynamic res = jsonDecode(body);
      for(var item in res){
        AlertModel _model = AlertModel.fromJSON(item);
        try{
          if(_model.location !=null){
            List<String> latLng  = _model.location.split(";");
            LatLng _l = LatLng(double.parse(latLng.first),double.parse(latLng.last));
            if(_l.latitude !=0.0 && _l.longitude != 0.0){
              ApiResponse addresses = await LocationService().getLocationName(_l);
              _model.locationAddress = addresses.message;
            }else{
              _model.locationAddress = "";
            }
          }else{
            _model.locationAddress = "";
          }
        }catch(e){
          print("Error "+_model.location);
          _model.locationAddress = "";
        }
        alerts.add(_model);
      }
      }catch(e){
       print(e);
      }
      return ApiResponse(statusCode: 200, message: alerts);
    }else
      return ApiResponse(statusCode: 404, message: "failed to load");
  }
}