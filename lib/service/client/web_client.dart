import 'dart:convert';
import 'dart:io';

import 'package:bdfleet/storage/shared_pref_manager.dart';

class WebApiClient{
  static final String BASE_URL = "https://prod.yuma-technology.co.uk:8443/yuma/";
  static final String IMAGE_URL = "https://cloud.bdfleet.com/images/";
  static WebApiClient getInstance = WebApiClient();
  HttpClient getHttpClient() {
    HttpClient webClient = HttpClient();
    webClient.connectionTimeout = Duration(seconds: 50);
    webClient.idleTimeout = Duration(seconds: 100);
    webClient.userAgent ="BDFLEET";
    webClient.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);
    return webClient;
  }
  post(formData, urlPath) async {
    try {
      HttpClientRequest request =
      await getHttpClient().postUrl(Uri.parse(BASE_URL + urlPath));
      request.headers.set('content-type', 'application/json');
      request.headers.set('Accept', 'application/json');
      request.add(utf8.encode(formData));
      return await request.close();
    } catch (e) {
      return null;
    }
  }
  put(formData, urlPath) async {
    String session = await SharedPrefManager.getToken();
    try {
      HttpClientRequest request =
      await getHttpClient().putUrl(Uri.parse(BASE_URL + urlPath));
      request.headers.set('content-type', 'application/json');
      request.headers.set('Accept', 'application/json');
      request.headers.set('YumaSession', session);
      request.add(utf8.encode(formData));
      return await request.close();
    } catch (e) {
      return null;
    }
  }

  get(urlPath) async {
    String session = await SharedPrefManager.getToken();
    try {
      print(BASE_URL + urlPath);
      HttpClientRequest request = await getHttpClient().getUrl(Uri.parse(BASE_URL + urlPath));
      request.headers.set('content-type', 'application/json');
      request.headers.set('Accept', 'application/json');
      request.headers.set('YumaSession', session);
      return await request.close();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
  // postWithoutBody(urlPath) async {
  //   try {
  //     HttpClientRequest request =
  //     await getHttpClient().postUrl(Uri.parse(baseURL + urlPath));
  //     request.headers.set('content-type', 'application/json');
  //     request.headers.set('Accept', 'application/json');
  //     return await request.close();
  //   } catch (e) {
  //     return null;
  //   }
  // }
  // patch(formData,urlPath) async {
  //   try {
  //     HttpClientRequest request =
  //     await getHttpClient().patchUrl(Uri.parse(baseURL + urlPath));
  //     request.headers.set('content-type', 'application/json');
  //     request.add(utf8.encode(formData));
  //     return await request.close();
  //   } catch (e) {
  //     print(e.toString());
  //     return null;
  //   }
  // }
  // delete(urlPath) async {
  //   try {
  //     HttpClientRequest request =
  //     await getHttpClient().deleteUrl(Uri.parse(baseURL + urlPath));
  //     request.headers.set('content-type', 'application/json');
  //     return await request.close();
  //   } catch (e) {
  //     print(e.toString());
  //     return null;
  //   }
  // }
}