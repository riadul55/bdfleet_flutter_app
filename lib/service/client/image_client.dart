import 'dart:convert';
import 'dart:io';

class ImageApiClient{
  static final String IMAGE_URL = "https://cloud.bdfleet.com/images/"; //cloud server
  static ImageApiClient getInstance = ImageApiClient();
  HttpClient getHttpClient() {
    HttpClient webClient = HttpClient();
    webClient.connectionTimeout = Duration(seconds: 15);
    webClient.idleTimeout = Duration(seconds: 50);
    webClient.userAgent ="BDFLEET";
    webClient.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);
    return webClient;
  }
  // post(formData, urlPath) async {
  //   try {
  //     HttpClientRequest request =
  //     await getHttpClient().postUrl(Uri.parse(baseURL + urlPath));
  //     request.headers.set('content-type', 'application/json');
  //     request.headers.set('Accept', 'application/json');
  //     request.add(utf8.encode(formData));
  //     return await request.close();
  //   } catch (e) {
  //     return null;
  //   }
  // }
  //
  // get(urlPath) async {
  //   try {
  //     HttpClientRequest request = await getHttpClient().getUrl(Uri.parse(baseURL + urlPath));
  //     request.headers.set('content-type', 'application/json');
  //     return await request.close();
  //   } catch (e) {
  //     print(e.toString());
  //     return null;
  //   }
  // }
  // postWithoutBody(urlPath) async {
  //   try {
  //     HttpClientRequest request =
  //     await getHttpClient().postUrl(Uri.parse(baseURL + urlPath));
  //     request.headers.set('content-type', 'application/json');
  //     request.headers.set('Accept', 'application/json');
  //     return await request.close();
  //   } catch (e) {
  //     return null;
  //   }
  // }
  // patch(formData,urlPath) async {
  //   try {
  //     HttpClientRequest request =
  //     await getHttpClient().patchUrl(Uri.parse(baseURL + urlPath));
  //     request.headers.set('content-type', 'application/json');
  //     request.add(utf8.encode(formData));
  //     return await request.close();
  //   } catch (e) {
  //     print(e.toString());
  //     return null;
  //   }
  // }
  // delete(urlPath) async {
  //   try {
  //     HttpClientRequest request =
  //     await getHttpClient().deleteUrl(Uri.parse(baseURL + urlPath));
  //     request.headers.set('content-type', 'application/json');
  //     return await request.close();
  //   } catch (e) {
  //     print(e.toString());
  //     return null;
  //   }
  // }
}