import 'dart:convert';

import 'package:bdfleet/model/device_model_local.dart';
import 'package:shared_preferences/shared_preferences.dart';
class SharedPrefManager{
  static Future setString(key,value) async {
    SharedPreferences myData = await SharedPreferences.getInstance();
    myData.setString(key, value);
  }
  static Future<String> getString(_key) async {
    SharedPreferences myData = await SharedPreferences.getInstance();
    return myData.getString(_key);
  }
  static Future setInt(key,int value) async {
    SharedPreferences myData = await SharedPreferences.getInstance();
    myData.setInt(key, value);
  }
  static Future<int> getInt(_key) async {
    SharedPreferences myData = await SharedPreferences.getInstance();
    return myData.getInt(_key);
  }
  static void setToken(String token) async {
    SharedPreferences myData = await SharedPreferences.getInstance();
    myData.setString('token', token);
  }

  static Future<String> getToken() async {
    SharedPreferences myData = await SharedPreferences.getInstance();
    String token = myData.getString('token');
    return token;
  }
  static void setUserId(String userId) async {
    SharedPreferences myData = await SharedPreferences.getInstance();
    myData.setString('userId', userId);
  }

  static Future<String> getUserId() async {
    SharedPreferences myData = await SharedPreferences.getInstance();
    String userId = myData.getString('userId');
    return userId;
  }
  static void saveAllDevice(List<DeviceModelLocal> allDevices) async {
    List<String> deviceList = [];
    allDevices.forEach((element) {
     deviceList.add(jsonEncode(DeviceModelLocal().toJSON(element)));
    });
    SharedPreferences myData = await SharedPreferences.getInstance();
    myData.setStringList('devices', deviceList);
  }
  static Future<List<DeviceModelLocal>> getAllDevice() async {
    SharedPreferences myData = await SharedPreferences.getInstance();
    List<String> deviceString = myData.getStringList('devices');
    List<DeviceModelLocal> allDevices = [];
    deviceString.forEach((element) {
      allDevices.add(DeviceModelLocal.fromJSON(jsonDecode(element)));
    });
    return allDevices;
  }
  static Future<bool> logOut() async {
    SharedPreferences myData = await SharedPreferences.getInstance();
    myData.clear();
    return myData.clear();
  }
}