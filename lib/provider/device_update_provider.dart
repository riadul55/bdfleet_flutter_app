import 'dart:convert';
import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/device_model.dart';
import 'package:bdfleet/model/device_model_local.dart';
import 'package:bdfleet/service/apis/device_update.dart';
import 'package:bdfleet/service/apis/profile.dart';
import 'package:bdfleet/service/helper/reponse.dart';
import 'package:bdfleet/storage/shared_pref_manager.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'dart:ui' as ui;

import 'package:location/location.dart';

class DeviceUpdate extends ChangeNotifier{
  LocationData locationData;
  List<DeviceModelLocal> allDevices = [];
  List<DeviceModelLocal> onlineDevices = [];
  List<DeviceModelLocal> offlineDevices = [];
  List<DeviceModelLocal> inActiveDevices = [];
  List<DeviceModelLocal> expiredDevices = [];
  bool detailsBottom = false;

  start() async{
        ApiResponse response =  await ProfileService().getProfileInfo();
        if(response.statusCode == 200){
          profile = response.message;
          var data = jsonDecode(profile.profileInfo.geofence_DCCarea);
          polyLatLng = [];
          data["coordinates"][0].forEach((e){
           LatLng latLng = LatLng(e[1], e[0]);
           polyLatLng.add(latLng);
          });
          polygonList.add(
            Polygon(
              points: polyLatLng,
              color: Colors.black.withOpacity(.1),
              borderColor: Colors.red,
              borderStrokeWidth: 3
            )
          );
        }
        notifyListeners();
    }
  updateAll() async{
    ApiResponse response = await DeviceUpdateService().getUpdate();
    if(response.statusCode == 200){
      List<DeviceUpdateModel> allUpdates = response.message;
      allDevices = allDevicesData;
      for(int j = 0 ; j< allUpdates.length;j++){
        var element = allUpdates[j];
        for(int i = 0 ;i<allDevices.length;i++){
          if(allDevices[i].id == element.deviceId){
            int dif = await Future.value(DateTime.now().difference(element.lastTimestamp).inSeconds);
            int sec = dif % 60;
            int min = Duration(seconds: dif).inMinutes;

            allDevices[i].lat = element.lat;
            allDevices[i].speed = element.speed;
            allDevices[i].runningStatus = element.runningStatus;
            allDevices[i].lon = element.lon;
            allDevices[i].idleTime = formatDuration(Duration(seconds: dif));
            allDevices[i].engine = dif >=300?EngineStatus.off:element.engine;
            allDevices[i].timestamp = element.timestamp;
            allDevices[i].lastTimestamp = element.lastTimestamp;
            allDevices[i].locationName = element.locationName;
            allDevices[i].status = element.status.toString();
            allDevices[i].life_cyle = element.lifeCycle;
            allDevices[i].heading = element.heading;
            allDevices[i].millage = element.millage;
            allDevices[i].type_id = element.typeID;
            allDevices[i].gsmSignal = element.gsmSignal;
            allDevices[i].gpsSatellite = element.gpsSatellite;
            allDevices[i].battery = element.battery;
          }
        }
      }
      onlineDevices = allDevices.where((element) => element.engine == EngineStatus.on).toList();
      offlineDevices = allDevices.where((element) => element.engine == EngineStatus.off).toList();
      expiredDevices = allDevices.where((element) => element.status == "0").toList();
      notifyListeners();
    }
  }
  getDeviceImage(device){
    return Container(
      margin: EdgeInsets.only(right: 5),
      child: device.vehicleType == VehicleType.car?getCarImage(device):device.vehicleType == VehicleType.bike?getBikeImage(device):device.vehicleType == VehicleType.cng?getCNGImage(device):getTruckImage(device),
    );
  }
  getCarImage(device){
    return Image.asset(device.engine==EngineStatus.off?"assets/images/item_car_off.png":device.runningStatus == RunningStatus.running?"assets/images/item_car_on.png":
    device.runningStatus == RunningStatus.slow?"assets/images/item_car_slow.png":
    device.runningStatus == RunningStatus.over?"assets/images/item_car_over.png":"assets/images/item_car_idle.png",
      height: 40,
      width: 40,
    );
  }
  getBikeImage(device){
    return Image.asset(device.engine==EngineStatus.off?"assets/images/item_bike_off.png":device.runningStatus == RunningStatus.running?"assets/images/item_bike_on.png":
    device.runningStatus == RunningStatus.slow?"assets/images/item_bike_slow.png":
    device.runningStatus == RunningStatus.over?"assets/images/item_bike_over.png":"assets/images/item_bike_idle.png",
      height: 40,
      width: 40,
    );
  }
  getCNGImage(device){
    return Image.asset(device.engine==EngineStatus.off?"assets/images/item_cng_off.png":device.runningStatus == RunningStatus.running?"assets/images/item_cng_on.png":
    device.runningStatus == RunningStatus.slow?"assets/images/item_cng_slow.png":
    device.runningStatus == RunningStatus.over?"assets/images/item_cng_over.png":"assets/images/item_cng_idle.png",
      height: 40,
      width: 40,
    );
  }
  getTruckImage(device){
    return Image.asset(device.engine==EngineStatus.off?"assets/images/item_truck_off.png":device.runningStatus == RunningStatus.running?"assets/images/item_car_on.png":
    device.runningStatus == RunningStatus.slow?"assets/images/item_car_slow.png":
    device.runningStatus == RunningStatus.over?"assets/images/item_car_over.png":"assets/images/item_car_idle.png",
      height: 40,
      width: 40,
    );
  }
  static String formatDuration(Duration d) {
    var seconds = d.inSeconds;
    final days = seconds~/Duration.secondsPerDay;
    seconds -= days*Duration.secondsPerDay;
    final hours = seconds~/Duration.secondsPerHour;
    seconds -= hours*Duration.secondsPerHour;
    final minutes = seconds~/Duration.secondsPerMinute;
    seconds -= minutes*Duration.secondsPerMinute;

    final List<String> tokens = [];
    if (days != 0) {
      tokens.add('${days}d');
    }
    if (tokens.isNotEmpty || hours != 0){
      tokens.add('${hours}h');
    }
    if (tokens.isNotEmpty || minutes != 0) {
      tokens.add('${minutes}min');
    }
    tokens.add('${seconds}sec');

    return tokens.join(' ');
  }

  // Future<BitmapDescriptor> getMarkerIcon(String imagePath, Size size,double rotation) async {
  //
  //   final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
  //   final Canvas canvas = Canvas(pictureRecorder);
  //   final Paint tagPaint = Paint()..color = Colors.white;
  //   // Oval for the image
  //   Rect oval = Rect.fromCenter(
  //     width: 120,
  //     height: 120,
  //     center: Offset(60,60),
  //   );
  //   // Add image
  //   ui.Image image = await getImageFromPath(imagePath); // Alterna
  //   ui.Image _img = await rotatedImage(image: image,angle: rotation);// tively use your own method to get the image
  //   paintImage(canvas: canvas, image: _img, rect: oval, fit: BoxFit.fitWidth);
  //   canvas.drawRRect(
  //       RRect.fromRectXY(
  //           Rect.fromCenter(
  //               width: 140,
  //               height: 40,
  //               center: Offset(70,120)),
  //           0,0
  //       ),
  //       tagPaint);
  //   // Add tag text
  //   TextPainter textPainter = TextPainter(
  //     textDirection: TextDirection.ltr,
  //     textAlign: TextAlign.center,
  //
  //   );
  //   textPainter.text = TextSpan(
  //     text: ' 12-8547 ',
  //     style: TextStyle(
  //       fontSize: 30.0, color: Colors.black,
  //       fontWeight: FontWeight.bold,
  //     ),
  //   );
  //
  //   textPainter.layout();
  //   textPainter.paint(
  //       canvas,
  //       Offset(0, 100)
  //   );
  //   // Convert canvas to image
  //   final ui.Image markerAsImage = await pictureRecorder.endRecording().toImage(
  //       size.width.toInt(),
  //       size.height.toInt()
  //   );
  //
  //
  //   // Convert image to bytes
  //   final ByteData byteData = await markerAsImage.toByteData(format: ui.ImageByteFormat.png);
  //   final Uint8List uint8List = byteData.buffer.asUint8List();
  //
  //   return BitmapDescriptor.fromBytes(uint8List);
  // }

  // Future<ui.Image> getImageFromPath(String imagePath) async {
  //
  //   //String fullPathOfImage = await getFileData(imagePath);
  //
  //   //File imageFile = File(fullPathOfImage);
  //   ByteData bytes = await rootBundle.load(imagePath);
  //   Uint8List imageBytes = bytes.buffer.asUint8List();
  //   //Uint8List imageBytes = imageFile.readAsBytesSync();
  //
  //   final Completer<ui.Image> completer = new Completer();
  //
  //   ui.decodeImageFromList(imageBytes, (ui.Image img) {
  //     return completer.complete(img);
  //   });
  //   //print("COMPLETERR DONE Full path of image is"+imagePath);
  //   return completer.future;
  // }
  // Future<ui.Image> rotatedImage({ui.Image image, double angle}) async{
  //   var pictureRecorder = ui.PictureRecorder();
  //   Canvas canvas = Canvas(pictureRecorder);
  //
  //   final double r = sqrt(image.width * image.width + image.height * image.height) / 2;
  //   final alpha = atan(image.height / image.width);
  //   final beta = alpha + angle;
  //   final shiftY = r * sin(beta);
  //   final shiftX = r * cos(beta);
  //   final translateX = image.width / 2 - shiftX;
  //   final translateY = image.height / 2 - shiftY;
  //   canvas.translate(translateX, translateY);
  //   canvas.rotate(angle);
  //   canvas.drawImage(image, Offset.zero, Paint());
  //
  //   return await pictureRecorder.endRecording().toImage(image.width, image.height);
  // }
}