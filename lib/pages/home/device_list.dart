import 'dart:async';

import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/provider/device_update_provider.dart';
import 'package:bdfleet/storage/shared_pref_manager.dart';
import 'package:bdfleet/view/device_list_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DeviceList extends StatefulWidget {
  @override
  _DeviceListState createState() => _DeviceListState();
}

class _DeviceListState extends State<DeviceList> with TickerProviderStateMixin{
  TabController tabController;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(initialIndex: 0, length: 5, vsync: this);

  }
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Consumer<DeviceUpdate>(
          builder: (context,data,child){

            return PreferredSize(
                child:TabBar(
                  controller: tabController,
                  labelColor: appsSubColor,
                  unselectedLabelColor: Colors.black,
                  indicatorColor: appsSubColor,
                  isScrollable: true,
                  tabs: [
                    Tab(
                      child: Text("ALL(${data.allDevices.length})"),
                    ),
                    Tab(
                      child: Text("ONLINE(${data.onlineDevices.length})"),
                    ),
                    Tab(
                      child: Text("OFFLINE(${data.offlineDevices.length})"),
                    ),
                    Tab(
                      child: Text("INACTIVE(${data.inActiveDevices.length})"),
                    ),
                    Tab(
                      child: Text("EXPIRE(${data.expiredDevices.length})"),
                    ),
                  ],

                ),
                preferredSize: Size.fromHeight(30.0)
            );
          },),
        Flexible(
          child: TabBarView(
              controller: tabController,
              children: [
                Container(
                  child:Consumer<DeviceUpdate>(
                    builder: (context,data,child){
                      return ListView(
                        children: data.allDevices.map((e){
                          return DeviceListModel(device: e,);
                        }).toList()
                      );
                    },
                  ),
                ),
                Container(
                  child:Consumer<DeviceUpdate>(
                    builder: (context,data,child){
                      return ListView(
                          children: data.onlineDevices.map((e){
                            return DeviceListModel(device: e,);
                          }).toList()
                      );
                    },
                  ),
                ),
                Container(
                  child:Consumer<DeviceUpdate>(
                    builder: (context,data,child){
                      return ListView(
                          children: data.offlineDevices.map((e){
                            return DeviceListModel(device: e,);
                          }).toList()
                      );
                    },
                  ),
                ),
                Icon(
                    Icons.eleven_mp
                ),
                Container(
                  child:Consumer<DeviceUpdate>(
                    builder: (context,data,child){
                      return ListView(
                          children: data.expiredDevices.map((e){
                            return DeviceListModel(device: e,);
                          }).toList()
                      );
                    },
                  ),
                ),
              ]
          ),
        )
      ],
    );
  }
}
