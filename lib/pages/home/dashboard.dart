import 'package:bdfleet/extra/loader.dart';
import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/device_model_local.dart';
import 'package:bdfleet/provider/device_update_provider.dart';
import 'package:bdfleet/service/apis/device_alert.dart';
import 'package:bdfleet/service/helper/reponse.dart';
import 'package:bdfleet/view/bottomView.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:provider/provider.dart';
import 'package:latlong/latlong.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  //Completer<GoogleMapController> _controller = Completer();
  MapController controller = MapController();
  bool routeEnabled = false;
  bool fenceEnabled = false;
  bool satelliteEnabled = false;
  bool myLocationViewEnabled = false;
  bool garageEnabled = false;
  bool loveEnabled = false;
  LatLng center = LatLng(0,0);
  bool controllerAttach = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }
  getCenter(List<LatLng> pts){
    var twicearea = 0.0;
    LatLng p1, p2;
    var f;
    var x = 0.0, y = 0.0;
    var nPts = pts.length;

    for(var i=0, j=nPts-1;i<nPts;j=i++) {
      p1=pts[i];
      p2=pts[j];
      twicearea+=p1.latitude*p2.longitude;
      twicearea-=p1.longitude*p2.latitude;

      f=p1.latitude*p2.longitude-p2.latitude*p1.longitude;

      x+=(p1.latitude+p2.latitude)*f;
      y+=(p1.longitude+p2.longitude)*f;
    }
    f=twicearea*3;
    return LatLng(x/f,y/f);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Consumer<DeviceUpdate>(
          builder: (context,data,child){
            List<String> garage = ['1','1'];
            if(profile !=null)
                garage= profile.profileInfo.garage.split(";");
            if(polyLatLng.length > 0){
              var _center = getCenter(polyLatLng);
              if(_center != center){
                  center = _center;
                  if(controllerAttach)
                  controller.move(center, 11);

              }
            }
            controllerAttach = true;
            return Stack(
              children:[
                FlutterMap(
                options: MapOptions(
                  zoom: center.latitude ==0? 2:11,
                  center: center,
                  onTap: (latLng){
                    print(latLng);
                  }
                ),
                mapController: controller,
                layers: [
                  TileLayerOptions(
                    urlTemplate: "http://{s}.google.com/vt/lyrs=${satelliteEnabled?'s':'m'}${routeEnabled?',traffic':''}&x={x}&y={y}&z={z}",
                    subdomains: ['mt0','mt1','mt2','mt3'],
                    /* h = roads only , m = standard roadmap, p = terrain, r = somehow altered roadmap, s = satellite only, t = terrain only, y = hybrid*/
                  ),
                  PolygonLayerOptions(
                    polygons: polygonList.toList()
                  ),
                  MarkerLayerOptions(
                    markers: data.allDevices.map((element){
                      return Marker(
                        width: 50.0,
                        height: 50.0,
                        point: LatLng(double.parse(element.lat),double.parse(element.lon)),
                        builder: (ctx) =>
                            RotationTransition(
                              turns: AlwaysStoppedAnimation(element.heading / 360),
                              child: SizedBox(
                                height: 50,
                                width: 20,
                                child: Container(
                                  child: FlatButton(
                                    splashColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    onPressed: (){
                                      showBottomSheet(
                                          context: context,
                                          backgroundColor: Colors.transparent,
                                          elevation: 0,
                                          builder: (context)=>DashBoardBottomSheet(element)
                                      );
                                    },
                                    padding: EdgeInsets.only(left: 0,right: 0,top: 0,bottom: 0),
                                    child: Image.asset(
                                      "assets/images/ic_yuma_cdoor.png",
                                      color: element.engine == EngineStatus.off?Colors.red:element.runningStatus == RunningStatus.running?Colors.greenAccent[400]:
                                      element.runningStatus == RunningStatus.slow?appsMainColor:appsSubColor,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                      );
                    }).toList()
                  ),
                  MarkerLayerOptions(
                    markers: data.allDevices.map((element){
                      return Marker(
                        height: 85.0,
                        width: 70,
                        point: LatLng(double.parse(element.lat),double.parse(element.lon)),
                        builder:(ctx)=>
                            Stack(
                              alignment: AlignmentDirectional.center,
                              children: [
                                RotationTransition(
                                  turns: AlwaysStoppedAnimation(45/ 360),
                                  alignment : Alignment.center,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    color:Colors.white,
                                    alignment:Alignment.center,
                                    padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                    margin:EdgeInsets.only(top: 35,left: 35),
                                  ),
                                ),
                                Container(
                                  alignment:Alignment.center,
                                  decoration:BoxDecoration(
                                      color:Colors.white,
                                      borderRadius: BorderRadius.all(Radius.circular(5))
                                  ),
                                  padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                  margin:EdgeInsets.only(top: 65),
                                  child: FlatButton(
                                    padding: EdgeInsets.only(left: 0,right: 0),
                                    onPressed: (){
                                      showBottomSheet(
                                          context: context,
                                          backgroundColor: Colors.transparent,
                                          elevation: 0,
                                          builder: (context)=>DashBoardBottomSheet(element)
                                      );},
                                    child: Text(element.description.substring(element.description.length==null?"loading":element.description.length-7,element.description.length),
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),

                      );
                    }).toList()
                  ),
                  MarkerLayerOptions(
                    markers: [
                      Marker(
                        height: 85.0,
                        width: 70,
                        point: LatLng(double.parse(garage[0]),double.parse(garage[1])),
                        builder:(ctx)=>
                            Stack(
                              alignment: AlignmentDirectional.center,
                              children: [
                                Container(
                                  height: 20,
                                  width: 20,
                                  decoration: BoxDecoration(
                                      color: Colors.blue,
                                      shape: BoxShape.circle
                                  ),

                                ),
                                RotationTransition(
                                  turns: AlwaysStoppedAnimation(45/ 360),
                                  alignment : Alignment.center,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    color:Colors.white,
                                    alignment:Alignment.center,
                                    padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                    margin:EdgeInsets.only(top: 35,left: 35),
                                  ),
                                ),
                                Container(
                                  alignment:Alignment.center,
                                  decoration:BoxDecoration(
                                    color:Colors.white,
                                    borderRadius: BorderRadius.all(Radius.circular(5)),

                                  ),
                                  padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                  margin:EdgeInsets.only(top: 65),
                                  child: Text("Garage",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),

                      ),
                      Marker(
                        height: 85.0,
                        width: 50,
                        point: currentLocation,
                        builder:(ctx)=>
                            Stack(
                              alignment: AlignmentDirectional.center,
                              children: [
                                Container(
                                  height: 20,
                                  width: 20,
                                  decoration: BoxDecoration(
                                      color: Colors.red,
                                      shape: BoxShape.circle
                                  ),

                                ),
                                RotationTransition(
                                  turns: AlwaysStoppedAnimation(45/ 360),
                                  alignment : Alignment.center,
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    color:Colors.white,
                                    alignment:Alignment.center,
                                    padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                    margin:EdgeInsets.only(top: 35,left: 35),
                                  ),
                                ),
                                Container(
                                  alignment:Alignment.center,
                                  decoration:BoxDecoration(
                                    color:Colors.white,
                                    borderRadius: BorderRadius.all(Radius.circular(5)),

                                  ),
                                  padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                  margin:EdgeInsets.only(top: 65),
                                  child: Text("You",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),

                      )
                    ]
                  ),

                ],
              ),
                Positioned(
                    right: 10,
                    top: 0,
                    child: Container(
                      child: Column(
                        children: [
                          Container(
                            height: 35,
                            width: 35,
                            color: Colors.black54,
                            margin: EdgeInsets.only(top: 10,),
                            child: IconButton(
                              padding: EdgeInsets.only(left:0,right: 0,top: 0,bottom: 0),
                              icon: Image.asset(
                                routeEnabled?"assets/images/ic_yuma_navigation_acc.png"
                                    :"assets/images/ic_yuma_navigation.png",
                                fit: BoxFit.cover,
                              ),
                              onPressed: (){
                                setState(() {
                                  routeEnabled = routeEnabled?false:true;
                                });
                              },
                            ),
                          ),
                          Container(
                            height: 35,
                            width: 35,
                            color: Colors.black54,
                            margin: EdgeInsets.only(top: 10,),
                            child: IconButton(
                                padding: EdgeInsets.only(left:0,right: 0,top: 0,bottom: 0),
                                icon: Image.asset(
                                  "assets/images/ic_yuma_street_view.png",
                                  fit: BoxFit.cover,
                                ),
                                onPressed: (){
                                  setState(() {
                                    satelliteEnabled = satelliteEnabled?false:true;
                                  });
                                }
                            ),
                          ),
                          Container(
                            color: Colors.black45,
                            height: 35,
                            width: 35,
                            margin: EdgeInsets.only(top: 10,),
                            child: IconButton(
                                padding: EdgeInsets.only(left:2,right: 2,top: 2,bottom: 2),
                                icon: Image.asset(
                                  "assets/images/ic_target.png",
                                  color:myLocationViewEnabled?appsSubColor:Colors.white,
                                  fit: BoxFit.cover,
                                ),
                                onPressed: (){
                                  setState(() {
                                    myLocationViewEnabled = myLocationViewEnabled?false:true;
                                    garageEnabled = false;
                                  });
                                  controller.move(myLocationViewEnabled?currentLocation:center,myLocationViewEnabled?15:11);
                                }
                            ),
                          ),
                          Container(
                            color: Colors.black45,
                            height: 35,
                            width: 35,
                            margin: EdgeInsets.only(top: 10),
                            child: IconButton(
                                padding: EdgeInsets.only(left:5,right: 5,top: 5,bottom: 5),
                                icon: Image.asset(
                                  "assets/images/ic_home.png",
                                  color: garageEnabled?appsSubColor:Colors.white,
                                  fit: BoxFit.cover,
                                ),
                                onPressed: (){
                                  setState(() {
                                    garageEnabled = garageEnabled?false:true;
                                    myLocationViewEnabled = false;
                                  });
                                  controller.move(garageEnabled?LatLng(double.parse(garage[0]),double.parse(garage[1])):center,garageEnabled?15:11);
                                }
                            ),
                          ),
                          Container(
                            color: Colors.black45,
                            height: 35,
                            width: 35,
                            margin: EdgeInsets.only(top: 10),
                            child: IconButton(
                                padding: EdgeInsets.only(left:5,right: 5,top: 5,bottom: 5),
                                icon: Icon(
                                  loveEnabled?Icons.favorite:Icons.favorite_border,
                                  color: appsSubColor,
                                ),
                                onPressed: (){
                                  setState(() {
                                    loveEnabled = loveEnabled?false:true;
                                  });
                                }
                            ),
                          )
                        ],
                      ),
                    )
                ),
              ]
            );
          },
        ),
      ),
    );
  }
}
