import 'dart:convert';

import 'package:bdfleet/extra/loader.dart';
import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/profile.dart';
import 'package:bdfleet/pages/login/login_page.dart';
import 'package:bdfleet/service/apis/profile.dart';
import 'package:bdfleet/service/client/web_client.dart';
import 'package:bdfleet/service/helper/reponse.dart';
import 'package:bdfleet/storage/shared_pref_manager.dart';
import 'package:bdfleet/view/change_password.dart';
import 'package:bdfleet/view/change_userName.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
class AddressPage extends StatefulWidget {
  @override
  _AddressState createState() => _AddressState();
}

class _AddressState extends State<AddressPage> {
  TextEditingController address1Controller =TextEditingController();
  TextEditingController address2Controller =TextEditingController();
  TextEditingController address3Controller =TextEditingController();
  TextEditingController city =TextEditingController();
  GlobalKey<FormState> _form = GlobalKey<FormState>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    address1Controller.text = profile.profileInfo.address1;
    address2Controller.text = profile.profileInfo.address2;
    address3Controller.text = profile.profileInfo.address3;
    city.text = profile.profileInfo.city;
    return Scaffold(
      appBar: AppBar(
        title: Text("Address"),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Form(
              key: _form,
              child: Card(
                elevation: 10,
                margin: EdgeInsets.only(left: 10,right: 10,bottom: 10),
                child: Container(
                  padding: EdgeInsets.only(left: 15,right: 15,top: 10,bottom: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 20,bottom: 5),
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          controller: address1Controller,
                          decoration:  InputDecoration(
                            hintText: "Address Line 1",
                            labelText: "Address Line 1",
                            labelStyle: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                            ),
                            hintStyle: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                            ),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                  width: 2,
                                )),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: appsMainColor,
                              ),
                            ),
                            contentPadding: EdgeInsets.only(left: 15, bottom: 0, top: 0, right: 15),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5,bottom: 5),
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          controller: address2Controller,
                          decoration:  InputDecoration(
                            hintText: "Address Line 2",
                            labelText: "Address Line 2",
                            labelStyle: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                            ),
                            hintStyle: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                            ),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                  width: 2,
                                )),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: appsMainColor,
                              ),
                            ),
                            contentPadding: EdgeInsets.only(left: 15, bottom: 0, top: 0, right: 15),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5,bottom: 5),
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          controller: address3Controller,
                          decoration:  InputDecoration(
                            hintText: "Address Line 3",
                            labelText: "Address Line 3",
                            labelStyle: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                            ),
                            hintStyle: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                            ),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                  width: 2,
                                )),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: appsMainColor,
                              ),
                            ),
                            contentPadding: EdgeInsets.only(left: 15, bottom: 0, top: 0, right: 15),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5,bottom: 5),
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.done,
                          controller: city,
                          decoration:  InputDecoration(
                            hintText: "City",
                            labelText: "City",
                            labelStyle: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                            ),
                            hintStyle: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                            ),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                  width: 2,
                                )),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: appsMainColor,
                              ),
                            ),
                            contentPadding: EdgeInsets.only(left: 15, bottom: 0, top: 0, right: 15),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20,bottom: 5),
                        height: 37,
                        width: 150,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Colors.black
                          )
                        ),
                        child: FlatButton(
                          onPressed: () {
                            updateAddress();
                          },
                          child: Text("Save"),
                        )
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  updateAddress() async{
    ProgressLoader.show();
    ApiResponse response = await ProfileService().updateAddress(address1Controller.text, address2Controller.text,address3Controller.text,city.text);
    ProgressLoader.hide();
    if(response.statusCode == 202){
      Get.showSnackbar(
          GetBar(
            message: "Success",
            backgroundColor: Colors.green,
            duration: Duration(seconds: 3),
          )
      );

    }else{
      Get.showSnackbar(
          GetBar(
            message: "Failed to update",
            backgroundColor: Colors.red,
            duration: Duration(seconds: 3),
          )
      );
    }
    setState(() {

    });
  }
}
