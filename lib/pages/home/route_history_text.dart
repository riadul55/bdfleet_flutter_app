import 'package:bdfleet/extra/loader.dart';
import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/device_model.dart';
import 'package:bdfleet/model/device_model_local.dart';
import 'package:bdfleet/pages/home/device_alerts.dart';
import 'package:bdfleet/pages/home/profile_page.dart';
import 'package:bdfleet/service/apis/location.dart';
import 'package:bdfleet/service/helper/reponse.dart';
import 'package:bdfleet/view/route_history_text_model_view.dart';
import 'package:bdfleet/view/route_history_text_model_view_idle.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:latlong/latlong.dart';

class RouteHistoryText extends StatefulWidget  {
  final DeviceModelLocal deviceModelLocal;
  final String fromTime,toTime;
  RouteHistoryText({this.deviceModelLocal,this.toTime,this.fromTime});

  @override
  _RouteHistoryTextState createState() => _RouteHistoryTextState();
}

class _RouteHistoryTextState extends State<RouteHistoryText> with TickerProviderStateMixin{

  List<DeviceUpdateModel> data = [];
  List<DeviceUpdateModel> idlePointsData = [];
  List<DeviceUpdateModel> parkPointsData = [];

  TabController tabController;
  GlobalKey<ScaffoldState> _scaffold = GlobalKey();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(initialIndex: 0, length: 3, vsync: this);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      getLocationHistory();
    });
  }
  getLocationHistory() async{
    ProgressLoader.show();
    ApiResponse response = await LocationService().getLocationHistory(widget.deviceModelLocal.id,DateTime.parse(widget.fromTime).toUtc().toString(),DateTime.parse(widget.toTime).toUtc().toString());
    if(response.statusCode == 200) {
      data = response.message;
      parkPointsData = [];
      idlePointsData = [];
      DeviceUpdateModel lastModel;
      for (int i = 0; i < data.length; i++) {
        int dif = 0;
        double speed = 0.0;

        if (i > 0) {
          lastModel = data[i - 1];
          try {
            speed = double.parse(lastModel.speed);
          } catch (e) {
            print(e);
          }
          if (speed < 1) {
            dif = data[i].timestamp
                .difference(lastModel.timestamp)
                .inSeconds;
          } else {

          }
        } else {
          lastModel = data[i];
        }
        if (dif > 300) {
          data[i].lastTimestamp = lastModel.timestamp;
          if (data[i].engine == EngineStatus.off) {
            parkPointsData.add(data[i]);
          } else if (data[i].engine == EngineStatus.on) {
            idlePointsData.add(data[i]);
          }
        }
      }
    }

    ProgressLoader.hide();
    setState(() {

    });
    for(int i = 0 ; i < data.length;i++){
      LatLng point = LatLng(
        double.parse(data[i].lon),
        double.parse(data[i].lat),
      );
      ApiResponse response = await LocationService().getLocationName(point);
      if(response.statusCode == 200){
        data[i].locationName = response.message;
        if(mounted)
        setState(() {

        });
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffold,
      appBar: AppBar(
        title: Text("Route History"),
        elevation: 0,
      ),
      body: TabBarView(
        controller: tabController,
        physics: NeverScrollableScrollPhysics(),
        children: [
          ListView.separated(
            itemCount: data.length,
            padding: EdgeInsets.only(left: 5,right: 5,top: 5,bottom: 5),
            separatorBuilder: (ctx,index){
              return Divider(
                height: 10,
                color: Colors.black.withOpacity(.4),
                thickness: 1,
              );
            },
            itemBuilder: (ctx,index){
              return RouteHistoryTextModel(
                model: data[index],
              );
            },
          ),
          ListView.separated(
            itemCount: parkPointsData.length,
            padding: EdgeInsets.only(left: 5,right: 5,top: 5,bottom: 5),
            separatorBuilder: (ctx,index){
              return Divider(
                height: 10,
                color: Colors.black.withOpacity(.4),
                thickness: 1,
              );
            },
            itemBuilder: (ctx,index){
              return RouteHistoryTextModelIdle(
                model: parkPointsData[index],
              );
            },
          ),
          ListView.separated(
            itemCount: idlePointsData.length,
            padding: EdgeInsets.only(left: 5,right: 5,top: 5,bottom: 5),
            separatorBuilder: (ctx,index){
              return Divider(
                height: 10,
                color: Colors.black.withOpacity(.4),
                thickness: 1,
              );
            },
            itemBuilder: (ctx,index){
              return RouteHistoryTextModelIdle(
                model: idlePointsData[index],
              );
            },
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        color: Colors.white,
        child: SizedBox(
          height: 60,
          child: TabBar(
            isScrollable: false,
            dragStartBehavior: DragStartBehavior.start,
            tabs: [
              Tab(
                text: "All",
              ),
              Tab(
                text: "Parked",
              ),
              Tab(
                text: "IDLE",
              ),
            ],
            labelStyle: TextStyle(
                fontSize: 15,
              fontWeight: FontWeight.bold
            ),
            labelColor: appsSubColor,
            unselectedLabelColor: Colors.black54,
            indicatorSize: TabBarIndicatorSize.tab,
            indicatorPadding: EdgeInsets.all(2),
            controller: tabController,
            indicator: UnderlineTabIndicator(
              borderSide: BorderSide(color: appsSubColor, width: 3),
            ),
          ),
        ),
      ),
    );
  }
}
class CustomListTile extends StatelessWidget{

  final String iconImage;
  final  String text;
  final onClick;

  CustomListTile(this.iconImage, this.text, this.onClick);
  @override
  Widget build(BuildContext context){
    //ToDO
    return Container(
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(
                  color: Colors.black,
                  width: .25
              )
          )
      ),
      child: FlatButton(
        splashColor: appsSubColor,
        onPressed: onClick,
        color: Colors.white,
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        padding: EdgeInsets.only(left:10,right:10,top:15,bottom: 15,),
        child: Row(
          children: [
            Container(
                margin: EdgeInsets.only(right: 20,left: 5),
                child: Image.asset(iconImage,height: 25, width: 25,)
            ),
            Container(
              child: Text(text,
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w700
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
