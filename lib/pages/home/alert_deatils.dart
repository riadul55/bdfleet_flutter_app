import 'dart:convert';

import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/alert_model.dart';
import 'package:bdfleet/model/device_model_local.dart';
import 'package:bdfleet/provider/device_update_provider.dart';
import 'package:bdfleet/view/bottomView.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:latlong/latlong.dart';
import 'package:intl/intl.dart';

class AlertDetails extends StatefulWidget {
  final AlertModel alertModel;
  final LatLng location;
  AlertDetails({@required this.alertModel,this.location});
  @override
  _AlertDetails createState() => _AlertDetails();
}

class _AlertDetails extends State<AlertDetails> {
  //Completer<GoogleMapController> _controller = Completer();
  MapController controller = MapController();

  bool routeEnabled = false;
  bool fenceEnabled = false;
  bool satelliteEnabled = false;
  bool myLocationViewEnabled = false;
  bool garageEnabled = false;
  bool loveEnabled = false;

  Set<Polygon> polygonList = {};
  List<LatLng> polyLatLng = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    DeviceModelLocal device;
    try {
      device = allDevicesData.singleWhere(
              (element) => element.id == widget.alertModel.device_id);

    } catch (e) {}

    var d = jsonDecode(device.geoFence);
    List<String> garage = profile.profileInfo.garage.split(";");
    polyLatLng = [];
    polygonList = {};
    d["coordinates"][0].forEach((e){
      LatLng latLng = LatLng(e[1], e[0]);
      polyLatLng.add(latLng);
    });
    polygonList.add(
        Polygon(
            points: polyLatLng,
            color: Colors.black.withOpacity(.1),
            borderColor: Colors.red,
            borderStrokeWidth: 3
        )
    );

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Details"),
        ),
        body: Stack(
              children: [
                FlutterMap(
                  options: MapOptions(
                      zoom: 15,
                      center: widget.location,
                      onTap: (latLng){
                        print(latLng);
                      }

                  ),
                  mapController: controller,
                  layers: [
                    TileLayerOptions(
                      urlTemplate: "http://{s}.google.com/vt/lyrs=${satelliteEnabled?'s':'m'}${routeEnabled?',traffic':''}&x={x}&y={y}&z={z}",
                      subdomains: ['mt0','mt1','mt2','mt3'],
                      /* h = roads only , m = standard roadmap, p = terrain, r = somehow altered roadmap, s = satellite only, t = terrain only, y = hybrid*/
                    ),
                    PolygonLayerOptions(
                        polygons: polygonList.toList()
                    ),
                    MarkerLayerOptions(
                        markers: [
                          Marker(
                            width: 50.0,
                            height: 50.0,
                            point: widget.location,
                            builder: (ctx) =>
                                RotationTransition(
                                  turns: AlwaysStoppedAnimation(device.heading / 360),
                                  child: SizedBox(
                                    height: 50,
                                    width: 20,
                                    child: Container(
                                      child: Image.asset(
                                        "assets/images/ic_yuma_cdoor.png",
                                        color: device.engine == EngineStatus.off?Colors.red:device.runningStatus == RunningStatus.running?Colors.greenAccent[400]:
                                        device.runningStatus == RunningStatus.slow?appsMainColor:appsSubColor,
                                      ),
                                    ),
                                  ),
                                ),
                          ),
                          Marker(
                            height: 85.0,
                            width: 70,
                            point: widget.location,
                            builder:(ctx)=>
                                Stack(
                                  alignment: AlignmentDirectional.center,
                                  children: [
                                    RotationTransition(
                                      turns: AlwaysStoppedAnimation(45/ 360),
                                      alignment : Alignment.center,
                                      child: Container(
                                        width: 15,
                                        height: 15,
                                        color:Colors.white,
                                        alignment:Alignment.center,
                                        padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                        margin:EdgeInsets.only(top: 35,left: 35),
                                      ),
                                    ),
                                    Container(
                                      alignment:Alignment.center,
                                      decoration:BoxDecoration(
                                          color:Colors.white,
                                          borderRadius: BorderRadius.all(Radius.circular(5))
                                      ),
                                      padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                      margin:EdgeInsets.only(top: 65),
                                      child: Text(device.description.substring(device.description.length==null?"loading":device.description.length-7,device.description.length),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),

                          ),
                          Marker(
                            height: 85.0,
                            width: 70,
                            point: LatLng(double.parse(garage[0]),double.parse(garage[1])),
                            builder:(ctx)=>
                                Stack(
                                  alignment: AlignmentDirectional.center,
                                  children: [
                                    Container(
                                      height: 20,
                                        width: 20,
                                      decoration: BoxDecoration(
                                          color: Colors.blue,
                                          shape: BoxShape.circle
                                      ),

                                    ),
                                    RotationTransition(
                                      turns: AlwaysStoppedAnimation(45/ 360),
                                      alignment : Alignment.center,
                                      child: Container(
                                        width: 15,
                                        height: 15,
                                        color:Colors.white,
                                        alignment:Alignment.center,
                                        padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                        margin:EdgeInsets.only(top: 35,left: 35),
                                      ),
                                    ),
                                    Container(
                                      alignment:Alignment.center,
                                      decoration:BoxDecoration(
                                          color:Colors.white,
                                          borderRadius: BorderRadius.all(Radius.circular(5)),

                                      ),
                                      padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                      margin:EdgeInsets.only(top: 65),
                                      child: Text("Garage",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),

                          ),
                          Marker(
                            height: 85.0,
                            width: 50,
                            point: currentLocation,
                            builder:(ctx)=>
                                Stack(
                                  alignment: AlignmentDirectional.center,
                                  children: [
                                    Container(
                                      height: 20,
                                      width: 20,
                                      decoration: BoxDecoration(
                                          color: Colors.red,
                                          shape: BoxShape.circle
                                      ),

                                    ),
                                    RotationTransition(
                                      turns: AlwaysStoppedAnimation(45/ 360),
                                      alignment : Alignment.center,
                                      child: Container(
                                        width: 15,
                                        height: 15,
                                        color:Colors.white,
                                        alignment:Alignment.center,
                                        padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                        margin:EdgeInsets.only(top: 35,left: 35),
                                      ),
                                    ),
                                    Container(
                                      alignment:Alignment.center,
                                      decoration:BoxDecoration(
                                        color:Colors.white,
                                        borderRadius: BorderRadius.all(Radius.circular(5)),

                                      ),
                                      padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                      margin:EdgeInsets.only(top: 65),
                                      child: Text("You",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),

                          )
                        ]

                    ),

                  ],
                ),
                Positioned(
                    right: 10,
                    top: 0,
                    child: Container(
                      child: Column(
                        children: [
                          Container(
                            height: 35,
                            width: 35,
                            color: Colors.black54,
                            margin: EdgeInsets.only(top: 10,),
                            child: IconButton(
                              padding: EdgeInsets.only(left:0,right: 0,top: 0,bottom: 0),
                              icon: Image.asset(
                                routeEnabled?"assets/images/ic_yuma_navigation_acc.png"
                                :"assets/images/ic_yuma_navigation.png",
                                fit: BoxFit.cover,
                              ),
                                onPressed: (){
                                  setState(() {
                                    routeEnabled = routeEnabled?false:true;
                                  });
                                },
                            ),
                          ),
                          Container(
                            height: 35,
                            width: 35,
                            color: Colors.black54,
                            margin: EdgeInsets.only(top: 10,),
                            child: IconButton(
                                padding: EdgeInsets.only(left:0,right: 0,top: 0,bottom: 0),
                                icon: Image.asset(
                                    "assets/images/ic_yuma_street_view.png",
                                  fit: BoxFit.cover,
                                ),
                                onPressed: (){
                                  setState(() {
                                    satelliteEnabled = satelliteEnabled?false:true;
                                  });
                                }
                            ),
                          ),
                          Container(
                            color: Colors.black45,
                            height: 35,
                            width: 35,
                            margin: EdgeInsets.only(top: 10,),
                            child: IconButton(
                              padding: EdgeInsets.only(left:2,right: 2,top: 2,bottom: 2),
                                icon: Image.asset(
                                    "assets/images/ic_target.png",
                                  color:myLocationViewEnabled?appsSubColor:Colors.white,
                                  fit: BoxFit.cover,
                                ),
                                onPressed: (){
                                  setState(() {
                                    myLocationViewEnabled = myLocationViewEnabled?false:true;
                                    garageEnabled = myLocationViewEnabled?false:true;
                                  });
                                  controller.move(myLocationViewEnabled?currentLocation:LatLng(double.parse(device.lat),double.parse(device.lon)), 15);
                                }
                            ),
                          ),
                          Container(
                            color: Colors.black45,
                            height: 35,
                            width: 35,
                            margin: EdgeInsets.only(top: 10),
                            child: IconButton(
                              padding: EdgeInsets.only(left:5,right: 5,top: 5,bottom: 5),
                                icon: Image.asset(
                                    "assets/images/ic_home.png",
                                  color: garageEnabled?appsSubColor:Colors.white,
                                  fit: BoxFit.cover,
                                ),
                                onPressed: (){
                                  setState(() {
                                    garageEnabled = garageEnabled?false:true;
                                    myLocationViewEnabled = false;
                                  });
                                  controller.move(garageEnabled?LatLng(double.parse(garage[0]),double.parse(garage[1])):LatLng(double.parse(device.lat),double.parse(device.lon)), 15);
                                }
                            ),
                          ),
                        ],
                      ),
                    )
                ),
                Positioned(
                    bottom: 0,
                    child: AlertDetailsSheet(widget.alertModel)
                ),
              ],
            )
        ),
    );
  }
}
class AlertDetailsSheet extends StatelessWidget {
  final AlertModel alertModel;
  AlertDetailsSheet(this.alertModel);

  @override
  Widget build(BuildContext context) {
    return FittedBox(
              fit: BoxFit.fitWidth,
              alignment: Alignment.center,
              child: Container(
                color: Colors.white,
                width: Get.width,
                margin: EdgeInsets.only(bottom: Get.bottomBarHeight),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(top: 10,bottom: 10),
                      child: Text("Location",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),

                   Container(
                        margin: EdgeInsets.only(left: 20,right: 20,bottom: 20),
                        child: Text(alertModel.locationAddress)
                    ),
                  ],
                ),
              ),
    );
  }
}
