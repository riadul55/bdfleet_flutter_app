import 'dart:convert';

import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/device_model_local.dart';
import 'package:bdfleet/provider/device_update_provider.dart';
import 'package:bdfleet/service/apis/way_points.dart';
import 'package:bdfleet/service/helper/reponse.dart';
import 'package:bdfleet/view/bottomView.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'package:latlong/latlong.dart';

class Navigation extends StatefulWidget {
  final DeviceModelLocal deviceModelLocal;
  Navigation({@required this.deviceModelLocal});
  @override
  _Navigation createState() => _Navigation();
}
double distance;
double estTimeToArrive;

class _Navigation extends State<Navigation> {
  MapController controller = MapController();

  Set<Polygon> polygonList = {};
  List<LatLng> allPoints = [];
  LatLng lastPosition;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPoints();
  }
  getPoints() async{
    LatLng toLatLng ;
    try{
      toLatLng = LatLng(double.parse(widget.deviceModelLocal.lat),double.parse(widget.deviceModelLocal.lon));
    }catch(e){}
    ApiResponse response = await WayPointNavigation().getRouteWayPoints(currentLocation, toLatLng);
    if(response.statusCode == 200){
      var data  = response.message;
      allPoints = [];
      for(var d in data["geometry"]["coordinates"]){
        distance = data["distance"];
        estTimeToArrive = data["duration"];
        allPoints.add(LatLng(d[1],d[0]));
      }
    }
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Navigation"),
        ),
        body: Consumer<DeviceUpdate>(
          builder: (context, data, child) {

            DeviceModelLocal device;
            try {
              device = data.allDevices.singleWhere(
                  (element) => element.id == widget.deviceModelLocal.id);
            } catch (e) {}
            var d = jsonDecode(device.geoFence);
            List<String> garage = profile.profileInfo.garage.split(";");
            polyLatLng = [];
            polygonList = {};
            d["coordinates"][0].forEach((e){
              LatLng latLng = LatLng(e[1], e[0]);
              polyLatLng.add(latLng);
            });
            polygonList.add(
                Polygon(
                    points: polyLatLng,
                    color: Colors.black.withOpacity(.1),
                    borderColor: Colors.red,
                    borderStrokeWidth: 3
                )
            );
            LatLng devicePos = LatLng(double.parse(widget.deviceModelLocal.lat),double.parse(widget.deviceModelLocal.lon));
            if(lastPosition != devicePos){
              lastPosition = devicePos;
              getPoints();
            }
            //getPoints();
            return Stack(
              children: [
                FlutterMap(
                  options: MapOptions(
                      zoom: 13,
                      center: currentLocation,
                      onTap: (latLng){
                        print(latLng);
                      }

                  ),
                  mapController: controller,
                  layers: [
                    TileLayerOptions(
                      urlTemplate: "http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}",
                      subdomains: ['mt0','mt1','mt2','mt3'],
                      /* h = roads only , m = standard roadmap, p = terrain, r = somehow altered roadmap, s = satellite only, t = terrain only, y = hybrid*/
                    ),
                    PolygonLayerOptions(
                        polygons: polygonList.toList()
                    ),
                    PolylineLayerOptions(
                      polylines: [
                        Polyline(
                          points: allPoints,
                          color: Colors.blue,
                          strokeWidth: 5
                        )
                      ]
                    ),
                    MarkerLayerOptions(
                        markers: [
                           Marker(
                            width: 50.0,
                            height: 50.0,
                            point: devicePos,
                            builder: (ctx) =>
                                RotationTransition(
                                  turns: AlwaysStoppedAnimation(widget.deviceModelLocal.heading / 360),
                                  child: SizedBox(
                                    height: 50,
                                    width: 20,
                                    child: Container(
                                      child: Image.asset(
                                        "assets/images/ic_yuma_cdoor.png",
                                        color: widget.deviceModelLocal.engine == EngineStatus.off?Colors.red:widget.deviceModelLocal.runningStatus == RunningStatus.running?Colors.greenAccent[400]:
                                        widget.deviceModelLocal.runningStatus == RunningStatus.slow?appsMainColor:appsSubColor,
                                      ),
                                    ),
                                  ),
                                ),
                          ),
                          Marker(
                            height: 85.0,
                            width: 50,
                            point: currentLocation,
                            builder:(ctx)=>
                                Stack(
                                  alignment: AlignmentDirectional.center,
                                  children: [
                                    Container(
                                      height: 20,
                                      width: 20,
                                      decoration: BoxDecoration(
                                          color: Colors.red,
                                          shape: BoxShape.circle
                                      ),

                                    ),
                                    RotationTransition(
                                      turns: AlwaysStoppedAnimation(45/ 360),
                                      alignment : Alignment.center,
                                      child: Container(
                                        width: 15,
                                        height: 15,
                                        color:Colors.white,
                                        alignment:Alignment.center,
                                        padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                        margin:EdgeInsets.only(top: 35,left: 35),
                                      ),
                                    ),
                                    Container(
                                      alignment:Alignment.center,
                                      decoration:BoxDecoration(
                                        color:Colors.white,
                                        borderRadius: BorderRadius.all(Radius.circular(5)),

                                      ),
                                      padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                      margin:EdgeInsets.only(top: 65),
                                      child: Text("You",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),

                          ),
                        ]
                    ),
                    MarkerLayerOptions(
                        markers: [
                           Marker(
                            height: 85.0,
                            width: 70,
                            point: LatLng(double.parse(widget.deviceModelLocal.lat),double.parse(widget.deviceModelLocal.lon)),
                            builder:(ctx)=>
                                Stack(
                                  alignment: AlignmentDirectional.center,
                                  children: [
                                    RotationTransition(
                                      turns: AlwaysStoppedAnimation(45/ 360),
                                      alignment : Alignment.center,
                                      child: Container(
                                        width: 15,
                                        height: 15,
                                        color:Colors.white,
                                        alignment:Alignment.center,
                                        padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                        margin:EdgeInsets.only(top: 35,left: 35),
                                      ),
                                    ),
                                    Container(
                                      alignment:Alignment.center,
                                      decoration:BoxDecoration(
                                          color:Colors.white,
                                          borderRadius: BorderRadius.all(Radius.circular(5))
                                      ),
                                      padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                      margin:EdgeInsets.only(top: 65),
                                      child: FlatButton(
                                        padding: EdgeInsets.only(left: 0,right: 0),
                                        onPressed: (){
                                          showBottomSheet(
                                              context: context,
                                              backgroundColor: Colors.transparent,
                                              elevation: 0,
                                              builder: (context)=>DashBoardBottomSheet(widget.deviceModelLocal)
                                          );},
                                        child: Text(widget.deviceModelLocal.description.substring(widget.deviceModelLocal.description.length==null?"loading":widget.deviceModelLocal.description.length-7,widget.deviceModelLocal.description.length),
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),

                          )
                        ]
                    ),
                  ],
                ),
                Positioned(
                    bottom: 0,
                    child: NavigationBottomSheet(device)
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
class NavigationBottomSheet extends StatefulWidget {
  final DeviceModelLocal device;
  NavigationBottomSheet(this.device);
  @override
  _NavigationBottomSheetState createState() => _NavigationBottomSheetState();
}

class _NavigationBottomSheetState extends State<NavigationBottomSheet> {
  bool details = true;
  @override
  Widget build(BuildContext context) {
    return Consumer<DeviceUpdate>(
          builder: (ctx,data,_){
            var format = new DateFormat('yyyy-MM-dd hh:mm:ss a');
            double width = Get.width;
            return FittedBox(
              fit: BoxFit.fitWidth,
              alignment: Alignment.center,
              child: Container(
                color: Colors.white,
                width: width,
                margin: EdgeInsets.only(bottom: Get.bottomBarHeight),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              width:(width-40)/2,
                              alignment: Alignment.center,
                              child: Text(
                                "Last active",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: Colors.black
                                ),
                              )
                          ),
                          Container(
                            height: 25,
                            width: 25,
                            child: IconButton(
                                icon: Icon(details?Icons.keyboard_arrow_down:Icons.keyboard_arrow_up),
                                padding:EdgeInsets.only(left: 0,right: 0,top: 0,bottom: 0),
                                onPressed: (){
                                  setState(() {
                                    details =  details? false:true;
                                  });
                                }
                            ),
                          ),
                          Container(
                            width:(width-40)/2,
                            alignment: Alignment.center,
                            child: Text("Last Positioning",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              width:(width-40)/2,
                              alignment: Alignment.center,
                              child: Text(
                                format.format(widget.device.lastTimestamp.toLocal()),
                                style: TextStyle(
                                    color: Colors.black.withOpacity(.8)
                                ),
                              )
                          ),
                          SizedBox(
                            height: 25,
                            width: 25,
                          ),
                          Container(
                            width:(width-40)/2,
                            alignment: Alignment.center,
                            child: Text( format.format(widget.device.timestamp.toLocal()),
                              style: TextStyle(
                                  color: Colors.black.withOpacity(.8)
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      child: Text(widget.device.description,
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.black
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 2,bottom: 2),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                              alignment: Alignment.center,
                              child: Text(
                                widget.device.speed +" km/h \n "+
                                    (widget.device.engine == EngineStatus.off?"OFF":widget.device.runningStatus==RunningStatus.running?"Moving":
                                    widget.device.runningStatus==RunningStatus.slow?"Slow":
                                    widget.device.runningStatus==RunningStatus.over?"Over":"Idle"
                                    ),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.black.withOpacity(.8)
                                ),
                              )
                          ),
                          Container(
                              alignment: Alignment.center,
                              child: Text(
                                "Engine\n "+
                                    (widget.device.engine == EngineStatus.off?"OFF":"ON"
                                    ),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.black.withOpacity(.8)
                                ),
                              )
                          ),
                          Container(
                              alignment: Alignment.center,
                              child: Text(
                                "Distance\n${distance==null?0:distance < 1000? distance.toStringAsFixed(0)+" m":(distance/1000).toStringAsFixed(2)} km",
                                style: TextStyle(
                                    color: Colors.black.withOpacity(.8)
                                ),
                                textAlign: TextAlign.center,
                              )
                          ),
                          Container(
                              alignment: Alignment.center,
                              child: Text(
                                " ETA to Reach\n ${DeviceUpdate.formatDuration(Duration(seconds: estTimeToArrive.toInt()))}",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.black.withOpacity(.8)
                                ),
                              )
                          ),
                          Container(
                              alignment: Alignment.center,
                              child: Text(
                                "Charge\n "+ widget.device.battery.toString(),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.black.withOpacity(.8)
                                ),
                              )
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      height: 10,
                      color: Colors.black,
                    ),
                    !details?Container():Container(
                        margin: EdgeInsets.only(left: 20,right: 20,top: 0,bottom: 3),
                        height: 30,
                        child: FlatButton(
                          color: appsSubColor,
                          textColor: Colors.white,
                          disabledColor: appsSubColor,
                          disabledTextColor: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)
                          ),
                          onPressed:widget.device.locationName.isEmpty?null: () {
                            setState(() {
                              widget.device.locationName = "";
                              Provider.of<DeviceUpdate>(context,listen: false).updateAll();
                            });
                          },
                          child: Text(widget.device.locationName.isEmpty?"Loading":"Refresh"),
                        )
                    ),
                    !details?Container():Container(
                        margin: EdgeInsets.only(left: 20,right: 20,),
                        child: Text(widget.device.locationName)
                    ),
                    !details?Container():Divider(
                      height: 5,
                      color: Colors.black,
                    ),
                  ],
                ),
              ),
            );
          }
    );
  }
}
