import 'dart:async';
import 'dart:convert';
import 'package:bdfleet/extra/loader.dart';
import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/profile.dart';
import 'package:bdfleet/pages/home/dashboard.dart';
import 'package:bdfleet/pages/home/device_alerts.dart';
import 'package:bdfleet/pages/home/device_list.dart';
import 'package:bdfleet/pages/home/profile_page.dart';
import 'package:bdfleet/pages/login/login_page.dart';
import 'package:bdfleet/provider/device_update_provider.dart';
import 'package:bdfleet/service/apis/profile.dart';
import 'package:bdfleet/service/helper/reponse.dart';
import 'package:bdfleet/storage/shared_pref_manager.dart';
import 'package:bdfleet/view/bottom_route_history.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:bdfleet/service/apis/device_alert.dart';

class HomePage extends StatefulWidget  {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin{
  TabController tabController;
  String appBarTitle = "Home";
  Location location = new Location();
  GlobalKey<ScaffoldState> _scaffold = GlobalKey();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(initialIndex: 0, length: 4, vsync: this);
    getDeviceFromLocal();
  }
  getDeviceFromLocal() async{
    allDevicesData =  await SharedPrefManager.getAllDevice();
    var _locationData = await location.getLocation();
    ProgressLoader.show();
    Provider.of<DeviceUpdate>(context,listen: false).locationData = _locationData;
    Provider.of<DeviceUpdate>(context,listen: false).start();
    Provider.of<DeviceUpdate>(context,listen: false).updateAll();
    ApiResponse response = await ProfileService().getProfileInfo();
    if(response.statusCode == 200){
      profile = response.message;
    }
    ProgressLoader.hide();
    Timer.periodic(Duration(seconds: 10), (Timer t){
      if(mounted){
        Provider.of<DeviceUpdate>(context,listen: false).updateAll();
      }
    });
  }
  logOutNow() async{
    await SharedPrefManager.logOut();
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (ctx)=>LoginPage()));
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffold,
      appBar: AppBar(
        title: Text(appBarTitle),
        elevation: 0,
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.only(left: 0,right: 0),
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 20,left: 0,bottom: 10),
              decoration:BoxDecoration(
                  color: Colors.black.withOpacity(.05)
              ),
              child: Column(
                children: <Widget>[
                        Container(
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.all(10),
                                height: 100,
                                width: 100,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                        color: Colors.deepOrange,
                                        width: 2
                                    )
                                ),
                                child:Container(
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                          image: NetworkImage("https://images.indianexpress.com/2020/04/ramadan-mubarak-759.jpg"),
                                          fit: BoxFit.cover
                                      )
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(top: 10,bottom: 5),
                          child: Text("Afsar Rahman",style: TextStyle(
                              fontSize: 16,
                              color: appsMainColor,
                              fontWeight: FontWeight.w700
                          ),),
                        ),
                      ],
                    ),
                  ),
            CustomListTile("assets/images/yuma_ic_dashboard.png", 'Dashboard', (){
              Navigator.pop(context);
            appBarTitle = "Dashboard";
                tabController.animateTo(0);
                setState(() {

                });
            }),
            // CustomListTile("assets/images/yuma_ic_route_navigation.png", 'Arrival Estimation', ()=>{
            //
            // }),
            CustomListTile("assets/images/yuma_ic_route.png", 'Route History', (){
              Navigator.push(context, MaterialPageRoute(builder: (ctx)=>BottomRouteHistory()));
            }),
           // CustomListTile("assets/images/yuma_ic_geofence.png", 'Geo Fence', ()=>{}),
           // CustomListTile("assets/images/yuma_ic_reports.png", 'Reports', ()=>{}),
            CustomListTile("assets/images/yuma_ic_alert.png", 'Alerts', (){
              Navigator.pop(context);
            appBarTitle = "Alerts";
                getAllAlerts();
              tabController.animateTo(2);
              setState(() {

              });
            }),
          //  CustomListTile("assets/images/yuma_ic_lservice.png", 'Location Service', ()=>{}),
            CustomListTile("assets/images/yuma_ic_profile.png", 'Profile', (){
              Navigator.pop(context);
              appBarTitle = "Profile";
              tabController.animateTo(3);
              setState(() {

              });
            }),
         //   CustomListTile("assets/images/ic_yuma_account.png", 'Account', ()=>{}),
            CustomListTile("assets/images/yuma_ic_logout.png", 'Log out', (){
              logOutNow();
            }),
          ],

        ),
      ),
      body: TabBarView(
        controller: tabController,
        physics: NeverScrollableScrollPhysics(),
        children: [
          Dashboard(),
          DeviceList(),
          DeviceAlerts(),
          ProfilePage(),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
            shape: CircularNotchedRectangle(),
            color: Colors.white,
            child: SizedBox(
              height: 60,
              child: TabBar(
                isScrollable: false,
                dragStartBehavior: DragStartBehavior.start,
                onTap: (index){
                  switch(index){
                    case 1:
                      setState(() {appBarTitle = "Device List";});
                      break;
                    case 2:
                      setState(() {
                        appBarTitle = "Alerts";
                        getAllAlerts();
                      });
                      break;
                    case 3:
                    setState(() {appBarTitle = "Profile";});
                    break;
                    default:
                      setState(() {appBarTitle = "Home";});
                  }
                },
                tabs: [
                  Tab(
                      icon: Icon(Icons.home,size: 30,),
                  ),
                  Tab(
                    icon: Icon(Icons.format_list_bulleted),
                  ),
                  Tab(
                    icon: Icon(Icons.notifications),
                  ),
                  Tab(
                    icon: Icon(Icons.account_circle),
                  ),
                ],
                labelStyle: TextStyle(fontSize: 10),
                labelColor: appsSubColor,
                unselectedLabelColor: Colors.black54,
                indicatorSize: TabBarIndicatorSize.tab,
                indicatorPadding: EdgeInsets.all(2),
                controller: tabController,
                indicator: UnderlineTabIndicator(
                  borderSide: BorderSide(color: appsSubColor, width: 3),
                ),
              ),
            ),
      ),
    );
  }
  getAllAlerts() async{
    var fromDate = DateTime.now().toUtc().subtract(Duration(minutes: 10));
    var toTime = DateTime.now().toUtc();
    ProgressLoader.show();
    ApiResponse response = await DeviceAlertService().getAllAlerts(fromDate.toString(), toTime.toString());
    ProgressLoader.hide();
    if(response.statusCode == 200){
      allAlerts = response.message;
    }
    setState(() {

    });
  }
}
class CustomListTile extends StatelessWidget{

  final String iconImage;
  final  String text;
  final onClick;

  CustomListTile(this.iconImage, this.text, this.onClick);
  @override
  Widget build(BuildContext context){
    //ToDO
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Colors.black,
            width: .25
          )
        )
      ),
      child: FlatButton(
          splashColor: appsSubColor,
          onPressed: onClick,
          color: Colors.white,
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          padding: EdgeInsets.only(left:10,right:10,top:15,bottom: 15,),
          child: Row(
            children: [
              Container(
                  margin: EdgeInsets.only(right: 20,left: 5),
                  child: Image.asset(iconImage,height: 25, width: 25,)
              ),
              Container(
                child: Text(text,
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w700
                  ),
                ),
              )
            ],
          ),
      ),
    );
  }
}
