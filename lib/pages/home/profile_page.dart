import 'dart:convert';
import 'dart:io';

import 'package:bdfleet/extra/loader.dart';
import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/profile.dart';
import 'package:bdfleet/pages/home/address.dart';
import 'package:bdfleet/pages/login/login_page.dart';
import 'package:bdfleet/service/apis/profile.dart';
import 'package:bdfleet/service/client/web_client.dart';
import 'package:bdfleet/service/helper/reponse.dart';
import 'package:bdfleet/storage/shared_pref_manager.dart';
import 'package:bdfleet/view/change_password.dart';
import 'package:bdfleet/view/change_userName.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:get/get.dart';
class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  TextEditingController nameController =TextEditingController();
  TextEditingController emailController =TextEditingController();
  BuildContext _context;
  String profileImage;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    nameController.text = profile.profileInfo.first_name +" "+profile.profileInfo.last_name;
    emailController.text = profile.userName;
    profileImage = profile.profileInfo.image;
    _context = context;
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Column(
            children: <Widget>[
              Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.only(top: 20),
                    height: 100,
                    width: 100,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle,
                        border: Border.all(
                            color: Colors.deepOrange,
                            width: 2
                        )
                    ),
                    child:Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: profileImage!=null? NetworkImage(WebApiClient.IMAGE_URL+profileImage):
                              AssetImage("assets/images/ic_yuma_profile.png"),
                              fit: BoxFit.cover
                          )
                      ),
                    ),
                  ),
                ],
              ),
              FlatButton(
                onPressed: (){
                  changeProfilePic();
                },
                textColor: appsMainColor,
                padding: EdgeInsets.only(top: 0,bottom: 0,left: 0,right: 0),
                child: Text("Change",style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w700
                ),),
              ),
            ],
          ),
          Form(
            child: Card(
              elevation: 10,
              margin: EdgeInsets.only(left: 10,right: 10,bottom: 10),
              child: Container(
                padding: EdgeInsets.only(left: 15,right: 15,top: 10,bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 5),
                      child: Text("Account Details",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                          color: appsMainColor
                        ),
                      ),
                    ),
                    Divider(
                      color: appsMainColor.withOpacity(.5),
                      height: 10,
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5,bottom: 5),
                      child: TextFormField(
                        keyboardType: TextInputType.text,
                        textInputAction: TextInputAction.next,
                        readOnly: true,
                        onTap: changeName,
                        decoration:  InputDecoration(
                          hintText: "Full Name",
                          labelText: "Full Name",
                          labelStyle: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                          ),
                          hintStyle: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                          ),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  width: 2,
                              )),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: appsMainColor,
                            ),
                          ),
                          contentPadding: EdgeInsets.only(left: 15, bottom: 0, top: 0, right: 15),
                        ),
                        controller: nameController,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5,bottom: 5),
                      child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        textInputAction: TextInputAction.next,
                        readOnly: true,
                        decoration:  InputDecoration(
                          hintText: "Email",
                          labelText: "Email",
                          labelStyle: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                          ),
                          hintStyle: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700
                          ),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(width: 2)),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: appsMainColor,
                            ),
                          ),
                          contentPadding:
                          EdgeInsets.only(left: 15, bottom: 0, top: 0, right: 15),
                        ),
                        controller: emailController,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5,bottom: 5),
                      child: TextFormField(
                        readOnly: true,
                        onTap: (){
                          changePassword();
                        },
                        decoration:  InputDecoration(
                          hintText: "Password",
                          labelText: "Password",
                          labelStyle: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                          ),
                          hintStyle: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700
                          ),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(width: 2)),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: appsMainColor,
                            ),
                          ),
                          contentPadding: EdgeInsets.only(left: 15, bottom: 0, top: 0, right: 15),
                        ),
                        initialValue: "Change Password",
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Form(
            child: Card(
              elevation: 10,
              margin: EdgeInsets.only(left: 10,right: 10,bottom: 10),
              child: Container(
                padding: EdgeInsets.only(left: 15,right: 15,top: 10,bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 5),
                      child: Text("Address",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                          color: appsMainColor
                        ),
                      ),
                    ),
                    Divider(
                      color: appsMainColor.withOpacity(.5),
                      height: 10,
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5,bottom: 5),
                      child: TextFormField(
                        readOnly: true,
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => AddressPage()));
                        },
                        decoration:  InputDecoration(
                          hintText: "Change Address",
                          labelStyle: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                              color: appsMainColor,
                          ),
                          hintStyle: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700
                          ),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(width: 2)),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: appsMainColor,
                            ),
                          ),
                          contentPadding: EdgeInsets.only(left: 15, bottom: 0, top: 0, right: 15),
                        ),
                        initialValue: "Change Address",
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Card(
            elevation: 10,
            margin: EdgeInsets.only(left: 10,right: 10,bottom: 10),
            child: Container(
              padding: EdgeInsets.only(left: 2,right: 15,top: 10,bottom: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 13,bottom: 5),
                    child: Text("Advance",
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                          color: appsMainColor
                      ),
                    ),
                  ),
                  Divider(
                    color: appsMainColor.withOpacity(.5),
                    height: 10,
                  ),
               /*   FlatButton.icon(
                      onPressed: (){


                      },
                      label: Text(
                          "Add Garage",
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w600
                        ),
                      ),
                      icon: Icon(Icons.center_focus_weak,color: appsMainColor,),
                  ),

                */
                  Divider(
                    color: appsMainColor.withOpacity(.5),
                    height: 0,
                  ),
                  FlatButton.icon(
                    onPressed: (){},
                    label: Text(
                      "Language",
                      style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w600
                      ),
                    ),
                    icon: Icon(Icons.language,color: appsMainColor,),
                  ),
                  Divider(
                    color: appsMainColor.withOpacity(.5),
                    height: 0,
                  ),
                  FlatButton.icon(
                    onPressed: (){
                      logOutNow();
                    },
                    label: Text(
                      "Log out",
                      style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w600
                      ),
                    ),
                    icon: Icon(Icons.power_settings_new,color: Colors.red,),
                  ),
                  Divider(
                    color: appsMainColor.withOpacity(.5),
                    height: 0,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
  changeProfilePic() async{
    // FilePickerResult result = await FilePicker.platform.pickFiles();
    //
    // if(result != null) {
    //   File file = File(result.files.single.path);
    //   print(file.path);
    // } else {
    //
    // }
  }
  changePassword(){
    AlertDialog alert = AlertDialog(
      title: Text("Change Password"),
      content: ChangePassword(
        onChange: updatePassword,
      ),
      scrollable: true,
      contentPadding: EdgeInsets.only(bottom: 10,left: 20,right: 20,top: 15),
    );
    showDialog(context: context,builder: (ctx)=>alert);
  }
  changeName(){
    AlertDialog alert = AlertDialog(
      title: Text("Change Name"),
      content: ChangeName(
        onChange: updateUserName,
      ),
      scrollable: true,
      contentPadding: EdgeInsets.only(bottom: 10,left: 20,right: 20,top: 15),
    );
    showDialog(context: context,builder: (ctx)=>alert);
  }
  updateUserName(firstName,lastName) async{
    ProgressLoader.show();
    ApiResponse response = await ProfileService().updateProfile(profile.userName, firstName,lastName);
    ProgressLoader.hide();
    if(response.statusCode == 202){
      Get.showSnackbar(
          GetBar(
            message: "Success",
            backgroundColor: Colors.green,
            duration: Duration(seconds: 3),
          )
      );

    }else{
      Get.showSnackbar(
          GetBar(
            message: "Failed to update",
            backgroundColor: Colors.red,
            duration: Duration(seconds: 3),
          )
      );
    }
    setState(() {

    });
  }

  updatePassword(password) async{
    ProgressLoader.show();
    ApiResponse response = await ProfileService().updatePassword(profile.userName, password);
    ProgressLoader.hide();
    if(response.statusCode == 202){
      Get.showSnackbar(
          GetBar(
            message: "Success",
            backgroundColor: Colors.green,
            duration: Duration(seconds: 3),
          )
      );
    }else{
      Get.showSnackbar(
          GetBar(
            message: "Failed to update",
            backgroundColor: Colors.red,
            duration: Duration(seconds: 3),
          )
      );
    }
    setState(() {

    });
  }
  logOutNow() async{
    await SharedPrefManager.logOut();
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (ctx)=>LoginPage()));
  }
}
