import 'package:bdfleet/extra/loader.dart';
import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/alert_model.dart';
import 'package:bdfleet/model/device_model_local.dart';
import 'package:bdfleet/service/apis/device_alert.dart';
import 'package:bdfleet/service/helper/reponse.dart';
import 'package:bdfleet/storage/shared_pref_manager.dart';
import 'package:bdfleet/view/device_alert_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DeviceAlerts extends StatefulWidget {
  final DeviceModelLocal deviceModelLocal;
  DeviceAlerts({this.deviceModelLocal});
  @override
  _DeviceAlertsState createState() => _DeviceAlertsState();
}

class _DeviceAlertsState extends State<DeviceAlerts> with TickerProviderStateMixin{
  List<AlertModel> geoFenceAlerts = [];
  List<AlertModel> powerAlerts = [];
  List<AlertModel> speedAlerts = [];
  List<AlertModel> otherAlerts = [];
  List<AlertModel> acknowledgeAlerts = [];
  bool searchEnable = false;
  bool started = false;
  DateTime selectedDate = DateTime.now();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if(widget.deviceModelLocal != null){
        getDeviceAlerts();
      }
    });
  }

  getDeviceAlerts() async{
    var fromDate = DateTime.now().toUtc().subtract(Duration(minutes: 10));
    var toTime = DateTime.now().toUtc();
    ProgressLoader.show();
    ApiResponse response = await DeviceAlertService().getAlertsSingleDevice(fromDate.toString(), toTime.toString(),widget.deviceModelLocal.id);
    ProgressLoader.hide();
    if(response.statusCode == 200){
      allAlerts = response.message;
    }
    setState(() {

    });
  }
  @override
  Widget build(BuildContext context) {
        geoFenceAlerts = allAlerts.where((element) => element.type_id == 3).toList();
        powerAlerts = allAlerts.where((element) => element.type_id == 2).toList();
        speedAlerts = allAlerts.where((element) => element.type_id == 4).toList();
        acknowledgeAlerts = allAlerts.where((element) => element.acknowledged == 1).toList();
        otherAlerts =  allAlerts.toSet().difference(powerAlerts.toSet()).toList();
        otherAlerts =  otherAlerts.toSet().difference(powerAlerts.toSet()).toList();
        otherAlerts =  otherAlerts.toSet().difference(speedAlerts.toSet()).toList();
        return Scaffold(
        appBar: widget.deviceModelLocal == null?null:
        searchEnable?
        AppBar(
          elevation: 0,
          title: Row(
            children: [
              Flexible(
                child: Container(
                  child: TextFormField(
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 17
                    ),
                    textAlignVertical: TextAlignVertical.center,
                    decoration: InputDecoration(
                      hintText: "Search",
                      hintStyle: TextStyle(
                          color: Colors.white60
                      ),
                      contentPadding: EdgeInsets.only(left: 5,right: 5,top: 5,bottom: 5),
                    ),
                  ),
                ),
              ),
              IconButton(
                icon: Icon(
                  Icons.close,
                  color: Colors.white,
                ),
                onPressed: (){
                  setState(() {
                    searchEnable = false;
                  });
                },
              )
            ],
          ),
        ) :
        AppBar(
          elevation: 0,
          title: Text("Alerts"),
          actions: [
            IconButton(
                icon: Icon(
                  Icons.search,
                  color: Colors.white,
                ),
                onPressed: (){
                  setState(() {
                    searchEnable =true;
                  });
                }
            ),
            IconButton(
                icon: Icon(
                  Icons.calendar_today_outlined,
                  color: Colors.white,
                ),
                onPressed: (){
                  _selectDate(context);
                }
            ),
            PopupMenuButton(
                itemBuilder: (ctx){
                  return ["Acknowledge All","Report a problem","Alert Setting"].map((e){
                    return PopupMenuItem(
                        value: e,
                        child: Text(e));
                  }).toList();
                })
          ],
        ),
        body: DefaultTabController(  // Added
          length: 5,  // Added
          initialIndex: 0, //Added
          child: Column(
            children: [
              PreferredSize(
                  child: Material(
                    color: Colors.teal,
                    child: TabBar(
                      labelColor: Colors.white,
                      labelStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                      unselectedLabelColor: Colors.white70,
                      indicatorColor: appsSubColor,
                      isScrollable: true,
                      tabs: [
                        Tab(
                          child: Text("GEOFENCE"),
                        ),
                        Tab(
                          child: Text("POWER"),
                        ),
                        Tab(
                          child: Text("SPEED"),
                        ),
                        Tab(
                          child: Text("OTHERS"),
                        ),
                        Tab(
                          child: Text("ACKNOWLEDGEMENT"),
                        ),
                      ],

                    ),
                  ),preferredSize: Size.fromHeight(30.0)
              ),
              Flexible(
                child: TabBarView(
                    children: [

                      Container(
                        child: ListView(
                          children: geoFenceAlerts.map((e){
                            return DeviceAlertModelView(model: e);
                          }).toList(),
                        ),
                      ),
                      Container(
                        child: ListView(
                          children: powerAlerts.map((e){
                            return DeviceAlertModelView(model: e);
                          }).toList(),
                        ),
                      ),
                      Container(
                        child: ListView(
                          children: speedAlerts.map((e){
                            return DeviceAlertModelView(model: e);
                          }).toList(),
                        ),
                      ),
                      Container(
                        child: ListView(
                          children: otherAlerts.map((e){
                            return DeviceAlertModelView(model: e);
                          }).toList(),
                        ),
                      ),
                      Container(
                        child: ListView(
                          children: acknowledgeAlerts.map((e){
                            return DeviceAlertModelView(model: e);
                          }).toList(),
                        ),
                      ),
                    ]
                ),
              )
            ],
          ),
        ),
      );

  }
  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }
}
