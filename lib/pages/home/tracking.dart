import 'dart:convert';

import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/device_model_local.dart';
import 'package:bdfleet/pages/home/device_alerts.dart';
import 'package:bdfleet/pages/home/navigation.dart';
import 'package:bdfleet/pages/home/playback_location.dart';
import 'package:bdfleet/provider/device_update_provider.dart';
import 'package:bdfleet/view/bottomView.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:latlong/latlong.dart';
import 'package:intl/intl.dart';

class Tracking extends StatefulWidget {
  final DeviceModelLocal deviceModelLocal;
  Tracking({@required this.deviceModelLocal});
  @override
  _Tracking createState() => _Tracking();
}

class _Tracking extends State<Tracking> {
  //Completer<GoogleMapController> _controller = Completer();
  MapController controller = MapController();

  bool routeEnabled = false;
  bool fenceEnabled = false;
  bool satelliteEnabled = false;
  bool myLocationViewEnabled = false;
  bool garageEnabled = false;
  bool loveEnabled = false;

  Set<Polygon> polygonList = {};
  List<LatLng> polyLatLng = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Tracking"),
        ),
        body: Consumer<DeviceUpdate>(
          builder: (context, data, child) {
            DeviceModelLocal device;
            try {
              device = data.allDevices.singleWhere(
                  (element) => element.id == widget.deviceModelLocal.id);

            } catch (e) {}
            var d = jsonDecode(device.geoFence);
            List<String> garage = profile.profileInfo.garage.split(";");
            polyLatLng = [];
            polygonList = {};
            d["coordinates"][0].forEach((e){
              LatLng latLng = LatLng(e[1], e[0]);
              polyLatLng.add(latLng);
            });
            polygonList.add(
                Polygon(
                    points: polyLatLng,
                    color: Colors.black.withOpacity(.1),
                    borderColor: Colors.red,
                    borderStrokeWidth: 3
                )
            );
            return Stack(
              children: [
                FlutterMap(
                  options: MapOptions(
                      zoom: 15,
                      center: LatLng(double.parse(device.lat),double.parse(device.lon)),
                      onTap: (latLng){
                        print(latLng);
                      }

                  ),
                  mapController: controller,
                  layers: [
                    TileLayerOptions(
                      urlTemplate: "http://{s}.google.com/vt/lyrs=${satelliteEnabled?'s':'m'}${routeEnabled?',traffic':''}&x={x}&y={y}&z={z}",
                      subdomains: ['mt0','mt1','mt2','mt3'],
                      /* h = roads only , m = standard roadmap, p = terrain, r = somehow altered roadmap, s = satellite only, t = terrain only, y = hybrid*/
                    ),
                    PolygonLayerOptions(
                        polygons: polygonList.toList()
                    ),
                    MarkerLayerOptions(
                        markers: [
                          Marker(
                            width: 50.0,
                            height: 50.0,
                            point: LatLng(double.parse(device.lat),double.parse(device.lon)),
                            builder: (ctx) =>
                                RotationTransition(
                                  turns: AlwaysStoppedAnimation(device.heading / 360),
                                  child: SizedBox(
                                    height: 50,
                                    width: 20,
                                    child: Container(
                                      child: Image.asset(
                                        "assets/images/ic_yuma_cdoor.png",
                                        color: device.engine == EngineStatus.off?Colors.red:device.runningStatus == RunningStatus.running?Colors.greenAccent[400]:
                                        device.runningStatus == RunningStatus.slow?appsMainColor:appsSubColor,
                                      ),
                                    ),
                                  ),
                                ),
                          ),
                          Marker(
                            height: 85.0,
                            width: 70,
                            point: LatLng(double.parse(device.lat),double.parse(device.lon)),
                            builder:(ctx)=>
                                Stack(
                                  alignment: AlignmentDirectional.center,
                                  children: [
                                    RotationTransition(
                                      turns: AlwaysStoppedAnimation(45/ 360),
                                      alignment : Alignment.center,
                                      child: Container(
                                        width: 15,
                                        height: 15,
                                        color:Colors.white,
                                        alignment:Alignment.center,
                                        padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                        margin:EdgeInsets.only(top: 35,left: 35),
                                      ),
                                    ),
                                    Container(
                                      alignment:Alignment.center,
                                      decoration:BoxDecoration(
                                          color:Colors.white,
                                          borderRadius: BorderRadius.all(Radius.circular(5))
                                      ),
                                      padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                      margin:EdgeInsets.only(top: 65),
                                      child: Text(device.description.substring(device.description.length==null?"loading":device.description.length-7,device.description.length),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),

                          ),
                          Marker(
                            height: 85.0,
                            width: 70,
                            point: LatLng(double.parse(garage[0]),double.parse(garage[1])),
                            builder:(ctx)=>
                                Stack(
                                  alignment: AlignmentDirectional.center,
                                  children: [
                                    Container(
                                      height: 20,
                                        width: 20,
                                      decoration: BoxDecoration(
                                          color: Colors.blue,
                                          shape: BoxShape.circle
                                      ),

                                    ),
                                    RotationTransition(
                                      turns: AlwaysStoppedAnimation(45/ 360),
                                      alignment : Alignment.center,
                                      child: Container(
                                        width: 15,
                                        height: 15,
                                        color:Colors.white,
                                        alignment:Alignment.center,
                                        padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                        margin:EdgeInsets.only(top: 35,left: 35),
                                      ),
                                    ),
                                    Container(
                                      alignment:Alignment.center,
                                      decoration:BoxDecoration(
                                          color:Colors.white,
                                          borderRadius: BorderRadius.all(Radius.circular(5)),

                                      ),
                                      padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                      margin:EdgeInsets.only(top: 65),
                                      child: Text("Garage",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),

                          ),
                          Marker(
                            height: 85.0,
                            width: 50,
                            point: currentLocation,
                            builder:(ctx)=>
                                Stack(
                                  alignment: AlignmentDirectional.center,
                                  children: [
                                    Container(
                                      height: 20,
                                      width: 20,
                                      decoration: BoxDecoration(
                                          color: Colors.red,
                                          shape: BoxShape.circle
                                      ),

                                    ),
                                    RotationTransition(
                                      turns: AlwaysStoppedAnimation(45/ 360),
                                      alignment : Alignment.center,
                                      child: Container(
                                        width: 15,
                                        height: 15,
                                        color:Colors.white,
                                        alignment:Alignment.center,
                                        padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                        margin:EdgeInsets.only(top: 35,left: 35),
                                      ),
                                    ),
                                    Container(
                                      alignment:Alignment.center,
                                      decoration:BoxDecoration(
                                        color:Colors.white,
                                        borderRadius: BorderRadius.all(Radius.circular(5)),

                                      ),
                                      padding:EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 2),
                                      margin:EdgeInsets.only(top: 65),
                                      child: Text("You",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),

                          )
                        ]

                    ),

                  ],
                ),
                Positioned(
                    right: 10,
                    top: 0,
                    child: Container(
                      child: Column(
                        children: [
                          Container(
                            height: 35,
                            width: 35,
                            color: Colors.black54,
                            margin: EdgeInsets.only(top: 10,),
                            child: IconButton(
                              padding: EdgeInsets.only(left:0,right: 0,top: 0,bottom: 0),
                              icon: Image.asset(
                                routeEnabled?"assets/images/ic_yuma_navigation_acc.png"
                                :"assets/images/ic_yuma_navigation.png",
                                fit: BoxFit.cover,
                              ),
                                onPressed: (){
                                  setState(() {
                                    routeEnabled = routeEnabled?false:true;
                                  });
                                },
                            ),
                          ),
                          Container(
                            height: 35,
                            width: 35,
                            color: Colors.black54,
                            margin: EdgeInsets.only(top: 10,),
                            child: IconButton(
                                padding: EdgeInsets.only(left:0,right: 0,top: 0,bottom: 0),
                                icon: Image.asset(
                                    "assets/images/ic_yuma_street_view.png",
                                  fit: BoxFit.cover,
                                ),
                                onPressed: (){
                                  setState(() {
                                    satelliteEnabled = satelliteEnabled?false:true;
                                  });
                                }
                            ),
                          ),
                          Container(
                            color: Colors.black45,
                            height: 35,
                            width: 35,
                            margin: EdgeInsets.only(top: 10,),
                            child: IconButton(
                              padding: EdgeInsets.only(left:2,right: 2,top: 2,bottom: 2),
                                icon: Image.asset(
                                    "assets/images/ic_target.png",
                                  color:myLocationViewEnabled?appsSubColor:Colors.white,
                                  fit: BoxFit.cover,
                                ),
                                onPressed: (){
                                  setState(() {
                                    myLocationViewEnabled = myLocationViewEnabled?false:true;
                                    garageEnabled = false;
                                  });
                                  controller.move(myLocationViewEnabled?currentLocation:LatLng(double.parse(device.lat),double.parse(device.lon)), 15);
                                }
                            ),
                          ),
                          Container(
                            color: Colors.black45,
                            height: 35,
                            width: 35,
                            margin: EdgeInsets.only(top: 10),
                            child: IconButton(
                              padding: EdgeInsets.only(left:5,right: 5,top: 5,bottom: 5),
                                icon: Image.asset(
                                    "assets/images/ic_home.png",
                                  color: garageEnabled?appsSubColor:Colors.white,
                                  fit: BoxFit.cover,
                                ),
                                onPressed: (){
                                  setState(() {
                                    garageEnabled = garageEnabled?false:true;
                                    myLocationViewEnabled = false;
                                  });
                                  controller.move(garageEnabled?LatLng(double.parse(garage[0]),double.parse(garage[1])):LatLng(double.parse(device.lat),double.parse(device.lon)), 15);
                                }
                            ),
                          ),
                        ],
                      ),
                    )
                ),
                Positioned(
                    bottom: 0,
                    child: TrackingBottomSheet(device)
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
class TrackingBottomSheet extends StatefulWidget {
  final DeviceModelLocal device;
  TrackingBottomSheet(this.device);
  @override
  _TrackingBottomSheetState createState() => _TrackingBottomSheetState();
}

class _TrackingBottomSheetState extends State<TrackingBottomSheet> {
  bool details = false;
  @override
  Widget build(BuildContext context) {
    return Consumer<DeviceUpdate>(
          builder: (ctx,data,_){
            var format = new DateFormat('yyyy-MM-dd hh:mm:ss a');
            double width = Get.width;
            return FittedBox(
              fit: BoxFit.fitWidth,
              alignment: Alignment.center,
              child: Container(
                color: Colors.white,
                width: width,
                margin: EdgeInsets.only(bottom: Get.bottomBarHeight),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              width:(width-40)/2,
                              alignment: Alignment.center,
                              child: Text(
                                "Last active",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: Colors.black
                                ),
                              )
                          ),
                          Container(
                            height: 25,
                            width: 25,
                            child: IconButton(
                                icon: Icon(details?Icons.keyboard_arrow_down:Icons.keyboard_arrow_up),
                                padding:EdgeInsets.only(left: 0,right: 0,top: 0,bottom: 0),
                                onPressed: (){
                                  setState(() {
                                    details =  details? false:true;
                                  });
                                }
                            ),
                          ),
                          Container(
                            width:(width-40)/2,
                            alignment: Alignment.center,
                            child: Text("Last Positioning",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              width:(width-40)/2,
                              alignment: Alignment.center,
                              child: Text(
                                format.format(widget.device.lastTimestamp.toLocal()),
                                style: TextStyle(
                                    color: Colors.black.withOpacity(.8)
                                ),
                              )
                          ),
                          SizedBox(
                            height: 25,
                            width: 25,
                          ),
                          Container(
                            width:(width-40)/2,
                            alignment: Alignment.center,
                            child: Text( format.format(widget.device.timestamp.toLocal()),
                              style: TextStyle(
                                  color: Colors.black.withOpacity(.8)
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      child: Text(widget.device.description,
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.black
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 2,bottom: 2),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                              alignment: Alignment.center,
                              child: Text(
                                widget.device.speed +" km/h \n "+
                                    (widget.device.engine == EngineStatus.off?"OFF":widget.device.runningStatus==RunningStatus.running?"Moving":
                                    widget.device.runningStatus==RunningStatus.slow?"Slow":
                                    widget.device.runningStatus==RunningStatus.over?"Over":"Idle"
                                    ),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.black.withOpacity(.8)
                                ),
                              )
                          ),
                          Container(
                              alignment: Alignment.center,
                              child: Text(
                                "Engine\n "+
                                    (widget.device.engine == EngineStatus.off?"OFF":"ON"
                                    ),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.black.withOpacity(.8)
                                ),
                              )
                          ),
                          Container(
                              alignment: Alignment.center,
                              child: Text(
                                "GSM\n(${widget.device.gsmSignal})",
                                style: TextStyle(
                                    color: Colors.black.withOpacity(.8)
                                ),
                                textAlign: TextAlign.center,
                              )
                          ),
                          Container(
                              alignment: Alignment.center,
                              child: Text(
                                "GPS\n (${widget.device.gpsSatellite.toString()})",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.black.withOpacity(.8)
                                ),
                              )
                          ),
                          Container(
                              alignment: Alignment.center,
                              child: Text(
                                "Charge\n "+ widget.device.battery.toString(),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.black.withOpacity(.8)
                                ),
                              )
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      height: 10,
                      color: Colors.black,
                    ),
                    !details?Container():Container(
                        margin: EdgeInsets.only(left: 20,right: 20,top: 0,bottom: 3),
                        height: 30,
                        child: FlatButton(
                          color: appsSubColor,
                          textColor: Colors.white,
                          disabledColor: appsSubColor,
                          disabledTextColor: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)
                          ),
                          onPressed:widget.device.locationName.isEmpty?null: () {
                            setState(() {
                              widget.device.locationName = "";
                              Provider.of<DeviceUpdate>(context,listen: false).updateAll();
                            });
                          },
                          child: Text(widget.device.locationName.isEmpty?"Loading":"Refresh"),
                        )
                    ),
                    !details?Container():Container(
                        margin: EdgeInsets.only(left: 20,right: 20,),
                        child: Text(widget.device.locationName)
                    ),
                    !details?Container():Divider(
                      height: 5,
                      color: Colors.black,
                    ),
                    !details?Container():Container(
                      margin: EdgeInsets.only(top: 2,bottom: 2),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          CustomButton(
                            label: "Alerts",
                            buttonImage: Container(
                              height: 25,
                              width: 25,
                              child: Icon(
                                Icons.taxi_alert,
                                color: appsSubColor,
                              ),
                            ),
                            onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder: (ctx)=>DeviceAlerts(deviceModelLocal: widget.device)));
                            },
                          ),
                          CustomButton(
                            label: "Navigation",
                            buttonImage: Container(
                              height: 25,
                              width: 25,
                              child: Icon(
                                Icons.navigation,
                                color: appsSubColor,
                              ),
                            ),
                            onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder: (ctx)=>Navigation(deviceModelLocal: widget.device)));
                            },
                          ),
                          CustomButton(
                            label: "Power",
                            buttonImage: Container(
                              height: 25,
                              width: 25,
                              child: Icon(
                                Icons.power_settings_new_outlined,
                                color: appsSubColor,
                              ),
                            ),
                          ),
                          CustomButton(
                            label: "Play Back",
                            onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder: (ctx)=>LocationPlayBack(deviceModelLocal: widget.device)));
                            },
                            buttonImage: Container(
                              height: 25,
                              width: 25,
                              child: Icon(
                                Icons.alt_route,
                                color: appsSubColor,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
    );
  }
}
