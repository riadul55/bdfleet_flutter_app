import 'dart:async';
import 'dart:convert';

import 'package:bdfleet/extra/loader.dart';
import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/device_model.dart';
import 'package:bdfleet/model/device_model_local.dart';
import 'package:bdfleet/model/idle_point.dart';
import 'package:bdfleet/provider/device_update_provider.dart';
import 'package:bdfleet/service/apis/location.dart';
import 'package:bdfleet/service/apis/way_points.dart';
import 'package:bdfleet/service/helper/reponse.dart';
import 'package:bdfleet/view/bottomView.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:intl/intl.dart';

class LocationPlayBack extends StatefulWidget {
  final DeviceModelLocal deviceModelLocal;
  LocationPlayBack({@required this.deviceModelLocal});
  @override
  _LocationPlayBack createState() => _LocationPlayBack();
}

class _LocationPlayBack extends State<LocationPlayBack> {
  MapController controller = MapController();
  int playSpeed = 1,currentIndex = 0;
  bool playing = false;
  DeviceUpdateModel currentPoint = DeviceUpdateModel(
    deviceId: "-1",
    timestamp: DateTime.now(),
    speed: "0.0",
    distance: "0.0",
    lat: "0",
    lon: "0"

  );
  Timer timer;
  LatLng lastPosition;
  LatLng firstPosition;
  List<LatLng> allPoints = [];
  List<LatLng> polyPoints = [];
  List<LatLng> idlePoints = [];
  List<IdlePointModel> idlePointsData = [];
  List<LatLng> parkPoints = [];

  List<DeviceUpdateModel> data = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      getLocationHistory();
    });
  }
  startTimer(){
    timer = Timer.periodic(Duration(milliseconds: playSpeed==2?500:playSpeed==3?250:1000), (timer) {
      try {
        if (currentIndex != (allPoints.length - 1)) {
          setState(() {
            currentPoint = data[currentIndex];
          });
          LatLng point = LatLng(
            double.parse(currentPoint.lon),
            double.parse(currentPoint.lat),
          );
          controller.move(point, controller.zoom);
          polyPoints.add(point);
          int dif = 0;
          if (currentIndex > 0) {
            dif = currentPoint.timestamp
                .difference(data[currentIndex - 1].timestamp)
                .inSeconds;
          }
          if (currentPoint.engine == EngineStatus.off && dif > 600) {
            parkPoints.add(point);
          } else if (dif > 600) {
            idlePoints.add(point);
            idlePointsData.add(IdlePointModel(point: point,idleTime: dif));
          }
          currentIndex++;
        }else{
          timer.cancel();
          playing =false;
          setState(() {

          });
        }
      }catch(e){

      }
    });
  }
  @override
  void dispose() {
    if(timer != null){
      timer.cancel();
    }
    super.dispose();
  }

  getLocationHistory() async{
    ProgressLoader.show();
    ApiResponse response = await LocationService().getLocationHistory(widget.deviceModelLocal.id,DateTime.parse(DateTime.now().toString().substring(0,10)+" 00:00:01").toUtc().toString(),DateTime.now().toUtc().toString());
    if(response.statusCode == 200){
      data = response.message;
      data.forEach((element) {
        allPoints.add(
            LatLng(
                double.parse(element.lon),
                double.parse(element.lat),
            )
        );
      });
      lastPosition = allPoints[allPoints.length-1];
      firstPosition = allPoints[0];
    }

    ProgressLoader.hide();
    controller.move(firstPosition, 15);
    setState(() {

    });
  }
  onDragEnd(){
    polyPoints = [];
    parkPoints = [];
    idlePoints = [];
    DeviceUpdateModel lastModel;
    for(int i = 0 ; i < currentIndex;i++){
      LatLng point = LatLng(
        double.parse(data[i].lon),
        double.parse(data[i].lat),
      );
      polyPoints.add(point);
      int dif = 0;
      double speed = 0;

      if(i > 0){
        lastModel = data[i-1];
        try{
          speed = double.parse(lastModel.speed);
        }catch(e){

        }
        if(speed == 0){
          dif = data[currentIndex].timestamp.difference(lastModel.timestamp).inSeconds;
        }else{

        }
      }else{
        lastModel = data[i];
      }
      if(data[i].engine == EngineStatus.off && dif > 600){
        parkPoints.add(point);
      }else if(data[i].engine == EngineStatus.on && dif > 600){
        idlePoints.add(point);
        idlePointsData.add(IdlePointModel(point: point,idleTime: dif));

      }
    }
    setState(() {

    });
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Navigation"),),
        body: Column(
              children: [
                Flexible(
                  child: FlutterMap(
                    options: MapOptions(
                        zoom: 2,
                        center: LatLng(24.78736,91.751781),
                        onTap: (latLng){
                          print(latLng);
                        }

                    ),
                    mapController: controller,
                    layers: [
                      TileLayerOptions(
                        urlTemplate: "http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}",
                        subdomains: ['mt0','mt1','mt2','mt3'],
                        /* h = roads only , m = standard roadmap, p = terrain, r = somehow altered roadmap, s = satellite only, t = terrain only, y = hybrid*/
                      ),
                      PolylineLayerOptions(
                          polylines: [
                            Polyline(
                                points: polyPoints,
                                color: Colors.blue,
                                strokeWidth: 2
                            )
                          ]
                      ),
                      MarkerLayerOptions(
                          markers: [
                            Marker(
                              width: 50.0,
                              height: 50.0,
                              point: firstPosition,
                              builder: (ctx) =>
                                  Container(
                                    child: Icon(
                                        Icons.flag,
                                      size: 30,
                                      color: Colors.green,
                                    )
                                  ),
                            ),
                            Marker(
                              height: 85.0,
                              width: 50,
                              point: lastPosition,
                              builder:(ctx)=>
                                  Container(
                                    child:  Container(
                                        child: Icon(
                                          Icons.flag,
                                          size: 30,
                                          color: Colors.red,
                                        )
                                    ),
                                  ),

                            ),
                            Marker(
                              height: 85.0,
                              width: 50,
                              point: LatLng(
                                double.parse(currentPoint.lon),
                                double.parse(currentPoint.lat),
                              ),
                              builder:(ctx)=>
                                  RotationTransition(
                                    turns: AlwaysStoppedAnimation(currentPoint.heading / 360),
                                    child: SizedBox(
                                      height: 50,
                                      width: 20,
                                      child: Container(
                                        child: Image.asset(
                                          "assets/images/ic_yuma_cdoor.png",
                                          color: currentPoint.engine == EngineStatus.off?Colors.red:currentPoint.runningStatus == RunningStatus.running?Colors.greenAccent[400]:
                                          currentPoint.runningStatus == RunningStatus.slow?appsMainColor:appsSubColor,
                                        ),
                                      ),
                                    ),
                                  ),

                            ),

                          ]
                      ),
                      MarkerLayerOptions(
                          markers: parkPoints.map((e){
                            return Marker(
                              width: 50.0,
                              height: 50.0,
                              point: e,
                              builder: (ctx) =>
                                  Container(
                                      child: Image.asset("assets/images/ic_yuma_parking.png")
                                  ),
                            );
                          }).toList()
                      ),
                      MarkerLayerOptions(
                          markers: idlePoints.map((e){
                            return Marker(
                              width: 50.0,
                              height: 50.0,
                              point: e,
                              builder: (ctx) =>
                                  Container(
                                      child: Image.asset("assets/images/ic_yuma_idle.png")
                                  ),
                            );
                          }).toList()
                      ),
                    ],
                  ),
                ),
                Container(
                  color: Colors.white,
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          children: [
                            Container(
                              child: IconButton(
                                onPressed: () {
                                  if(playing){
                                    playing = false;
                                    if(timer != null){
                                      if(timer.isActive){
                                        timer.cancel();
                                      }
                                    }
                                  }else{
                                    playing =true;
                                    if(timer == null)
                                        startTimer();
                                    else if(!timer.isActive){
                                      startTimer();
                                    }
                                  }
                                  setState(() {

                                  });
                                },
                                color: appsSubColor,
                                icon: Icon(playing?Icons.pause_circle_filled:Icons.play_circle_outline),
                              ),
                            ),
                            Flexible(
                              child: Slider(
                                onChanged: (double value) {
                                  setState(() {
                                    currentIndex = value.toInt();

                                  });
                                },
                                onChangeEnd: (value){
                                  currentIndex = value.toInt();
                                  onDragEnd();
                                },
                                divisions: allPoints.length == 0 ?1:allPoints.length,
                                autofocus: true,
                                value: currentIndex.toDouble(),
                                activeColor: appsSubColor,
                                max: allPoints.length.toDouble(),
                                min: 0,
                              ),
                            ),
                            Container(
                              child: Text("X $playSpeed"),
                            ),
                            Container(
                              child: IconButton(
                                onPressed: () {
                                  if(timer != null)
                                      timer.cancel();
                                  playSpeed++;
                                  if(playSpeed > 3){
                                    playSpeed = 1;
                                  }
                                  setState(() {

                                  });
                                  startTimer();
                                },
                                color: appsSubColor,
                                icon: Icon(Icons.fast_forward),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Divider(
                        color: appsSubColor,
                        height: 3,
                        thickness: 2,
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10,bottom: 10,left: 10,right: 10),
                        child: Row(
                          children: [
                            Container(
                              child: Icon(
                                Icons.access_time_rounded,
                                color: appsSubColor,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 5,right: 15),
                              child: Column(
                                children: [
                                  Text(
                                      DateFormat('hh:mm:ss a').format(currentPoint.timestamp.toLocal())
                                  ),
                                  Text(
                                      DateFormat('yyyy-MM-dd').format(currentPoint.timestamp.toLocal())
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              child: Icon(
                                Icons.speed,
                                color: appsSubColor,
                              ),
                            ),
                            Container(
                              child: Text(currentPoint.speed +" km/h"),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 15,right: 5),
                              child: Icon(
                                Icons.settings_backup_restore,
                                color: appsSubColor,
                              ),
                            ),
                            Container(
                              child: Text(currentPoint.distance==null?"0":currentPoint.distance +" km"),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            )
        ),
    );
  }
}