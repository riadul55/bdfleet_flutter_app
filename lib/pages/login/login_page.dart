import 'package:bdfleet/extra/alert_failed_to_connect.dart';
import 'package:bdfleet/extra/loader.dart';
import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/pages/home/home_page.dart';
import 'package:bdfleet/service/apis/login.dart';
import 'package:bdfleet/service/helper/reponse.dart';
import 'package:bdfleet/storage/shared_pref_manager.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:latlong/latlong.dart';

// ignore: must_be_immutable
class LoginPage extends StatefulWidget{
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

   TextEditingController emailController = TextEditingController();

   TextEditingController passwordController = TextEditingController();

  BuildContext _context;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    askForPermission();
  }
  checkLogin() async{
    String userId = await SharedPrefManager.getUserId();
    String __email = await SharedPrefManager.getString("__userName");
      if(userId != null){
         var locationData = await Location.instance.getLocation();
         currentLocation = LatLng(locationData.latitude,locationData.longitude);

         Navigator.pushReplacement(context, MaterialPageRoute(builder: (ctx)=>HomePage()));
      }else{
        emailController.text = __email;
        setState(() {

        });
    }
  }
  askForPermission() async{
    if (await Permission.location.isRestricted) {
      await Permission.location.request().then((value){
        if(value.isGranted){
            checkLogin();
          }else{
            askForPermission();
          }
        }
      );
    }else{
      checkLogin();
    }
  }
  @override
  Widget build(BuildContext context) {
    _context = context;
    return Scaffold(
      key: scaffoldKey,
      appBar: PreferredSize(preferredSize: Size(0.0, 0.0),child: Container()),
      body:Container(
        padding: EdgeInsets.only(left: 15,right: 15,top: 10,bottom: 10),
        decoration: BoxDecoration(
          image:  DecorationImage(
            image: AssetImage(
                "assets/images/background.png",
            ),
            fit: BoxFit.cover
          )
        ),
        child: Form(
          key: formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                child: TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  textInputAction: TextInputAction.next,
                  style: TextStyle(

                  ),
                  textAlignVertical: TextAlignVertical.center,
                  enableSuggestions: true,
                  validator: (str){
                    if(str.isEmpty){
                      return "required";
                    }else{
                      return null;
                    }
                  },
                  decoration:  InputDecoration(
                    hintText: "Email",
                    labelText: "Email",
                    labelStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                    hintStyle: TextStyle(
                      fontWeight: FontWeight.w700
                    ),
                    border: OutlineInputBorder(borderSide: BorderSide(width: 2)),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: appsMainColor,
                      ),
                    ),
                    contentPadding:
                    EdgeInsets.only(left: 15, bottom: 5, top: 5, right: 15),
                  ),
                  controller: emailController,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10,bottom: 10),
                child: TextFormField(
                  keyboardType: TextInputType.visiblePassword,
                  textInputAction: TextInputAction.done,
                  validator: (str){
                    if(str.isEmpty){
                      return "required";
                    }else{
                      return null;
                    }
                  },
                  obscureText: true,
                  decoration:  InputDecoration(
                    hintText: "Password",
                    labelText: "Password",
                    labelStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                    hintStyle: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w700
                    ),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(width: 2)),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: appsMainColor,
                      ),
                    ),
                    contentPadding: EdgeInsets.only(left: 15, bottom: 0, top: 0, right: 15),
                  ),
                  controller: passwordController,
                ),
              ),
              FlatButton(
                  color: appsMainColor,
                  textColor: Colors.white,
                  padding: EdgeInsets.symmetric(vertical: 10),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)
                  ),
                  onPressed: (){
                    formKey.currentState.save();
                    var validate = formKey.currentState.validate();
                    if(validate){
                      loginNow();
                    }
                  },
                  child: Text("Login"),
              )
            ],
          ),
        ),
      ),
    );
  }

  loginNow() async{
    SharedPrefManager.setString("__userName", emailController.text);
    ProgressLoader.show();
    ApiResponse response = await LoginService().loginNow(emailController.text.trim(), passwordController.text.trim());
    ProgressLoader.hide();
    if(response.statusCode==200){
      //login success
      ProgressLoader.show();
      String userID = await SharedPrefManager.getUserId();
      ApiResponse response = await LoginService().getAllDeviceList(userID);
      SharedPrefManager.saveAllDevice(response.message);
      ProgressLoader.hide();
      checkLogin();
    }else if(response.statusCode==500){
      //failed to connect
      AlertFailedToConnect(_context).show("Failed to connect . please try again later");
    }else{
      scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(response.message,
              style: TextStyle(color: Colors.white,fontSize: 15,fontWeight: FontWeight.bold),
            ),
            backgroundColor: Colors.red,
            duration: Duration(seconds: 3),
          )
      );
    }
  }
}
