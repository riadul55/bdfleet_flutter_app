import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/device_model_local.dart';
import 'package:bdfleet/pages/home/route_history_text.dart';
import 'package:bdfleet/view/device_select.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
class BottomRouteHistory extends StatefulWidget {
  @override
  _BottomRouteHistoryState createState() => _BottomRouteHistoryState();
}

class _BottomRouteHistoryState extends State<BottomRouteHistory> {
  DeviceModelLocal selectedDevice;
  int reportType = 0;
  TextEditingController fromDate = TextEditingController();
  TextEditingController fromTime = TextEditingController();
  TextEditingController fullDay = TextEditingController();
  TextEditingController toDate = TextEditingController();
  TextEditingController toTime = TextEditingController();
  @override
  void initState() {
    DateTime today = DateTime.now();
    fromDate.text =  DateFormat('yyyy-MM-dd').format(today);
    fullDay.text =  DateFormat('yyyy-MM-dd').format(today);
    fromTime.text =  "00:00:00";
    toDate.text = DateFormat('yyyy-MM-dd').format(today);
    toTime.text = DateFormat('hh:mm:ss').format(today);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    if(selectedDevice == null){
      selectedDevice = allDevicesData[0];
    }
    return Scaffold(
      appBar: AppBar(
        title: Text("Route History"),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 30,right: 30),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          )
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
                padding: EdgeInsets.only(top: 10,bottom: 5),
                alignment: Alignment.center,
                child: Text(
                    "Route History",
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.black
                  ),
                )
            ),
            Divider(

            ),
            Container(
              margin: EdgeInsets.only(top: 10,bottom: 10),
              child: Text(
                  "Choose Your Device",
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.black.withOpacity(.7),
                    fontWeight: FontWeight.bold
                  ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: appsSubColor,
                  width: 1
                ),
                  borderRadius: BorderRadius.all(Radius.circular(5))
              ),
              child: FlatButton(
                onPressed: (){
                  openDeviceSelect();
                },
                child:Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                        selectedDevice.description
                    ),
                    Icon(Icons.arrow_drop_down)
                  ],
                )
              )
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Container(
                        height: 45,
                        decoration: BoxDecoration(

                            border: Border.all(
                                color: appsSubColor,
                                width: 1
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                        child: FlatButton(
                            onPressed: (){
                              var today = DateTime.now();
                              fromDate.text =  DateFormat('yyyy-MM-dd').format(today);
                              fullDay.text =  DateFormat('yyyy-MM-dd').format(today);
                              fromTime.text =  "00:00:00";
                              toDate.text = DateFormat('yyyy-MM-dd').format(today);
                              toTime.text = DateFormat('hh:mm:ss').format(today);
                              setState(() {
                                reportType = 0;
                              });
                            },
                            child:Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                    "Today"
                                ),
                                reportType==0?Icon(Icons.check_circle,color: appsSubColor,):Container()
                              ],
                            )
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 45,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: appsSubColor,
                                width: 1
                            ),
                          borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                        child: FlatButton(
                            onPressed: (){
                              var yesterday = DateTime.now().subtract(Duration(days: 1));
                              fromDate.text =  DateFormat('yyyy-MM-dd').format(yesterday);
                              fullDay.text =  DateFormat('yyyy-MM-dd').format(yesterday);
                              fromTime.text =  "00:00:00";
                              toDate.text = DateFormat('yyyy-MM-dd').format(yesterday);
                              toTime.text = DateFormat('hh:mm:ss').format(yesterday);
                              setState(() {
                                reportType = 1;
                              });                          },
                            child:Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                    "Yesterday"
                                ),
                                reportType==1?Icon(Icons.check_circle,color: appsSubColor,):Container()
                              ],
                            )
                        ),
                      ),
                    ),
                  ],
                )
            ),
            Container(
                margin: EdgeInsets.only(top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Container(
                        height: 45,
                        decoration: BoxDecoration(

                            border: Border.all(
                                color: appsSubColor,
                                width: 1
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                        child: FlatButton(
                            onPressed: (){
                              setState(() {
                                reportType = 2;
                              });                          },
                            child:Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                    "Time Range"
                                ),
                                reportType==2?Icon(Icons.check_circle,color: appsSubColor,):Container()
                              ],
                            )
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      child: Container(
                        height: 45,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: appsSubColor,
                                width: 1
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                        child: FlatButton(
                            onPressed: (){
                              setState(() {
                                reportType = 3;
                              });                          },
                            child:Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                    "Date Wise"
                                ),
                                reportType==3?Icon(Icons.check_circle,color: appsSubColor,):Container()
                              ],
                            )
                        ),
                      ),
                    ),
                  ],
                )
            ),
            reportType==2?Container(
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 10,bottom: 0),
                    child: Text(
                      "Choose Date And Time",
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.black.withOpacity(.7),
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 10),
                      height: 45,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width:50,
                            child: Text(
                              "From : ",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black.withOpacity(.7)
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              height: 40,
                              child: TextFormField(
                                controller: fromDate,
                                onTap: (){
                                  _selectDate(context, fromDate);
                                },
                                readOnly: true,
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(left: 10,bottom: 0),
                                    border: OutlineInputBorder()
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Container(
                              height: 40,
                              child: TextFormField(
                                controller: fromTime,
                                onTap: (){
                                  _selectTime(context,fromTime);
                                },
                                readOnly: true,
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(left: 10),
                                    border: OutlineInputBorder()
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 10),
                      height: 45,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width:50,
                            child: Text(
                              "To : ",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black.withOpacity(.7)
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              height: 40,
                              child: TextFormField(
                                controller: toDate,
                                onTap: (){
                                  _selectDate(context, toDate);
                                },
                                readOnly: true,
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(left: 10,bottom: 0),
                                    border: OutlineInputBorder()
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Container(
                              height: 40,
                              child: TextFormField(
                                controller: toTime,
                                onTap: (){
                                  _selectTime(context,toTime);
                                },
                                readOnly: true,
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(left: 10),
                                    border: OutlineInputBorder()
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                  ),
                ],
              ),
            ):
            reportType==3?Container(
              child: Container(
                  margin: EdgeInsets.only(top: 10),
                  height: 45,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width:100,
                        child: Text(
                          "Choose Date : ",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black.withOpacity(.7)
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Container(
                          height: 40,
                          child: TextFormField(
                            controller: fullDay,
                            onTap: (){
                              _selectDate(context, fullDay);
                            },
                            readOnly: true,
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(left: 10),
                                border: OutlineInputBorder()
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
              ),
            ):Container(),
            Container(
              margin: EdgeInsets.only(top: 10),
              height: 40,
              child: FlatButton(
                  onPressed: (){

                  },
                  textColor: Colors.white,
                  color: appsSubColor,
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25)
                  ),
                  child:Text(
                      "Route Show"
                  )
              ),
            ),
            Container(
                margin: EdgeInsets.only(top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Container(
                        height: 40,
                        decoration: BoxDecoration(
                            color: appsSubColor,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(25),
                              bottomLeft: Radius.circular(25),
                            )                      ),
                        child: FlatButton(
                            onPressed: (){},
                            textColor: Colors.white,
                            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                            child:Text(
                                "Play Back"
                            )
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        height: 40,
                        margin: EdgeInsets.only(left: 3,right: 3),
                        decoration: BoxDecoration(
                            color: appsSubColor
                        ),
                        child: FlatButton(
                            onPressed: (){

                            },
                            textColor: Colors.white,
                            child:Text(
                                "Park & Idle"
                            )
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        height: 40,
                        decoration: BoxDecoration(
                           color: appsSubColor,
                            borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(25),
                                  bottomRight: Radius.circular(25),
                                )
                        ),
                        child: FlatButton(
                            onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder:(ctx)=>RouteHistoryText(
                                fromTime: fromDate.text+" "+fromTime.text,
                                toTime: toDate.text+" "+toTime.text,
                                deviceModelLocal: selectedDevice,
                              )));
                            },
                            textColor: Colors.white,
                            child:Text(
                                "Text View"
                            )
                        ),
                      ),
                    ),
                  ],
                )
            ),

          ],
        ),
      ),
    );
  }
  Future<void> _selectDate(BuildContext context,TextEditingController controller) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now().subtract(Duration(days: 30)),
        lastDate: DateTime.now());
    if (picked != null)
      setState(() {
        controller.text = DateFormat('yyyy-MM-dd').format(picked);;
      });
  }
  Future<void> _selectTime(BuildContext context,TextEditingController controller) async {
    final TimeOfDay picked = await showTimePicker(
        context: context,
        initialTime: TimeOfDay(hour: 0, minute: 0),
        confirmText: "Select"
    );
    if (picked != null)
      setState(() {
        controller.text = "${picked.hour.toString().padLeft(2,"0")}:${picked.minute.toString().padLeft(2,"0")}:00";
      });
  }
  openDeviceSelect() async{
    Navigator.push(context,
        MaterialPageRoute(
            builder: (ctx)=>
                DeviceSelect(
                  selected: selectedDevice,
                  callWithData: (item){
                    setState(() {
                      selectedDevice = item;
                    });
                  },
                )
        )
    );
  }
}
