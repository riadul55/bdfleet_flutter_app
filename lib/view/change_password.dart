import 'package:bdfleet/extra/theme_Data.dart';
import 'package:flutter/material.dart';
typedef CallWithOne(x);
class ChangePassword extends StatefulWidget {
  final CallWithOne onChange;
  ChangePassword({this.onChange});
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  TextEditingController passwordController = TextEditingController();
  TextEditingController passwordConfirmController = TextEditingController();
  GlobalKey<FormState> _form = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _form,
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 5,bottom: 5),
            child: TextFormField(
              keyboardType: TextInputType.text,
              obscureText: true,
              textInputAction: TextInputAction.next,
              validator: (str){
                if(passwordController.text.isEmpty){
                  return "required";
                }else{
                  return null;
                }
              },
              decoration:  InputDecoration(
                hintText: "New Password",
                labelText: "New Password",
                labelStyle: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                ),
                hintStyle: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
                border: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 2,
                    )),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: appsMainColor,
                  ),
                ),
                contentPadding: EdgeInsets.only(left: 15, bottom: 0, top: 0, right: 15),
              ),
              controller: passwordController,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 5,bottom: 5),
            child: TextFormField(
              keyboardType: TextInputType.visiblePassword,
              obscureText: true,
              textInputAction: TextInputAction.done,
              validator: (str){
                if(passwordConfirmController.text != passwordController.text){
                  return "didn't match";
                }else{
                  return null;
                }
              },
              decoration:  InputDecoration(
                hintText: "Re-Type Password",
                labelText: "Re-Type Password",
                labelStyle: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
                hintStyle: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w700
                ),
                border: OutlineInputBorder(
                    borderSide: BorderSide(width: 2)),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: appsMainColor,
                  ),
                ),
                contentPadding:
                EdgeInsets.only(left: 15, bottom: 0, top: 0, right: 15),
              ),
              controller: passwordConfirmController,
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  textColor: appsMainColor,
                  child: Text("Back"),
                ),
                FlatButton(
                  onPressed: () {
                    bool result = _form.currentState.validate();
                    if(result){
                      Navigator.pop(context);
                      widget.onChange(passwordController.text.trim());
                    }
                  },
                  textColor: appsMainColor,
                  child: Text("Change"),
                ),
                ],
            ),
          )
        ],
      ),
    );
  }
}
