import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/device_model_local.dart';
import 'package:bdfleet/pages/home/device_alerts.dart';
import 'package:bdfleet/pages/home/tracking.dart';
import 'package:flutter/material.dart';

class DeviceListModel extends StatelessWidget {
  final DeviceModelLocal device;
  DeviceListModel({this.device});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 5,right: 5,top: 0),
      child: Card(
        elevation: 10,
        child: Container(
          padding:EdgeInsets.only(left: 5,right: 5,top: 5,bottom: 5),
          height: 100,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                      child: Row(
                        children: [
                          getDeviceImage(),
                          Text(device.description,
                          style: TextStyle(
                            fontWeight: FontWeight.w600
                          ),
                          ),
                        ],
                      )
                  ),
                  Column(
                    children: [
                      Container(
                        width: 100,
                        padding: EdgeInsets.only(left: 10,right: 10,top: 5,bottom: 5),
                        color: device.engine == EngineStatus.off?Colors.red:device.runningStatus == RunningStatus.running?Colors.greenAccent[400]:
                        device.runningStatus == RunningStatus.slow?appsMainColor:appsSubColor,
                        child: Text(device.engine == EngineStatus.off?device.idleTime : device.runningStatus == RunningStatus.idle?device.idleTime:"${device.speed}+ km/h",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 12
                          ),
                        ),
                      ),
                      Container(
                        width: 100,
                        padding: EdgeInsets.only(left: 5,right: 5,top: 5,bottom: 5),
                        color: Colors.black12,
                        child: Text(device.engine == EngineStatus.off?"OFF":device.runningStatus == RunningStatus.running?"Moving":device.runningStatus == RunningStatus.over?"Over":
                        device.runningStatus == RunningStatus.slow?"SLOW":"IDLE",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 12
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
              Divider(
                color: Colors.black54,
                height: 1,
              ),
              Container(
                height: 20,
                child: Row(
                  children: [
                    Flexible(
                        child: Container(
                          child: Row(
                            children: [
                              FlatButton(
                                onPressed: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (ctx)=>DeviceAlerts(deviceModelLocal: device)));
                                },
                                padding: EdgeInsets.only(left: 0),
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                child: Container(
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Icon(Icons.notifications_none,size: 20,color: appsMainColor,),
                                        Text(" Alerts",style:TextStyle(fontSize: 14),)
                                      ],
                                    ),
                                  ),
                              ),
                              FlatButton(
                                onPressed: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (ctx)=>Tracking(deviceModelLocal: device)));
                                },
                                padding: EdgeInsets.only(left: 0),
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                child: Container(
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Icon(Icons.location_on,size: 20,color: appsMainColor,),
                                      Text(" Tracking",style:TextStyle(fontSize: 14),)
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                    ),
                    FlatButton(
                      onPressed: (){},
                      padding: EdgeInsets.only(left: 0),
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      child: Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(Icons.power_settings_new,size: 20,color: Colors.red,),
                            Text(" Power",style:TextStyle(fontSize: 14, color: Colors.red,),)
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  getDeviceImage(){
    return Container(
      margin: EdgeInsets.only(right: 5),
      child: device.vehicleType == VehicleType.car?getCarImage():device.vehicleType == VehicleType.bike?getBikeImage():device.vehicleType == VehicleType.cng?getCNGImage():getTruckImage(),
    );
  }
  getCarImage(){
    return Image.asset(device.engine==EngineStatus.off?"assets/images/item_car_off.png":device.runningStatus == RunningStatus.running?"assets/images/item_car_on.png":
    device.runningStatus == RunningStatus.slow?"assets/images/item_car_slow.png":
    device.runningStatus == RunningStatus.over?"assets/images/item_car_over.png":"assets/images/item_car_idle.png",
        height: 40,
        width: 40,
    );
  }
  getBikeImage(){
    return Image.asset(device.engine==EngineStatus.off?"assets/images/item_bike_off.png":device.runningStatus == RunningStatus.running?"assets/images/item_bike_on.png":
    device.runningStatus == RunningStatus.slow?"assets/images/item_bike_slow.png":
    device.runningStatus == RunningStatus.over?"assets/images/item_bike_over.png":"assets/images/item_bike_idle.png",
      height: 40,
      width: 40,
    );
  }
  getCNGImage(){
    return Image.asset(device.engine==EngineStatus.off?"assets/images/item_cng_off.png":device.runningStatus == RunningStatus.running?"assets/images/item_cng_on.png":
    device.runningStatus == RunningStatus.slow?"assets/images/item_cng_slow.png":
    device.runningStatus == RunningStatus.over?"assets/images/item_cng_over.png":"assets/images/item_cng_idle.png",
      height: 40,
      width: 40,
    );
  }
  getTruckImage(){
    return Image.asset(device.engine==EngineStatus.off?"assets/images/item_truck_off.png":device.runningStatus == RunningStatus.running?"assets/images/item_car_on.png":
    device.runningStatus == RunningStatus.slow?"assets/images/item_car_slow.png":
    device.runningStatus == RunningStatus.over?"assets/images/item_car_over.png":"assets/images/item_car_idle.png",
      height: 40,
      width: 40,
    );
  }
}
