import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/device_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';


class RouteHistoryTextModelIdle extends StatelessWidget {
  final DeviceUpdateModel model;
  RouteHistoryTextModelIdle({this.model});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      child: Column(
        children: [
          Expanded(
            child: Row(
              children: [
                Container(
                    width: 60,
                    color: appsSubColor,
                    alignment: Alignment.center,
                    child: Text(DateFormat('dd\nMMM').format(model.timestamp.toLocal()),
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white
                      ),
                    )
                ),
                Expanded(
                    child:Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 10,right: 10),
                          child: Text(
                            "Duration : ${formatDuration(Duration(seconds:  model.timestamp.difference(model.lastTimestamp).inSeconds))}"
                            ,style: TextStyle(
                              color: Colors.black,fontWeight: FontWeight.w400
                          ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10,right: 10),
                          child: Text(
                            "Location : \n"
                                "${model.locationName==null?"":model.locationName.replaceAll(", Bangladesh","")}"
                            ,style: TextStyle(
                              color: Colors.black,fontWeight: FontWeight.w400
                          ),
                          ),
                        )
                      ],
                    ),
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(top: 2,bottom: 0),
            child: Text(
              DateFormat('hh:mm:ss a').format(model.timestamp.toLocal()),
              style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.bold
              ),
            ),
          )
        ],
      ),
    );
  }
  static String formatDuration(Duration d) {
    var seconds = d.inSeconds;
    final days = seconds~/Duration.secondsPerDay;
    seconds -= days*Duration.secondsPerDay;
    final hours = seconds~/Duration.secondsPerHour;
    seconds -= hours*Duration.secondsPerHour;
    final minutes = seconds~/Duration.secondsPerMinute;
    seconds -= minutes*Duration.secondsPerMinute;

    final List<String> tokens = [];
    if (days != 0) {
      tokens.add('${days}d');
    }
    if (tokens.isNotEmpty || hours != 0){
      tokens.add('${hours}h');
    }
    if (tokens.isNotEmpty || minutes != 0) {
      tokens.add('${minutes}min');
    }
    tokens.add('${seconds}sec');

    return tokens.join(' ');
  }
}
