import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/alert_model.dart';
import 'package:bdfleet/pages/home/alert_deatils.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:latlong/latlong.dart';


class DeviceAlertModelView extends StatefulWidget {
  final AlertModel model;
  DeviceAlertModelView({this.model});
  @override
  _DeviceAlertModelViewState createState() => _DeviceAlertModelViewState();
}

class _DeviceAlertModelViewState extends State<DeviceAlertModelView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 5,right: 5,top: 5),
      child: FlatButton(
        padding: EdgeInsets.only(left: 0,right: 0,top: 0,bottom: 0),
        onPressed: (){
          LatLng location = LatLng(0,0);
          if(widget.model.location != null){
            List<String> latLng  = widget.model.location.split(";");
            location= LatLng(double.parse(latLng.first),double.parse(latLng.last));
          }
          Navigator.push(context,
              MaterialPageRoute(
                  builder: (ctx)=>
                      AlertDetails(
                        alertModel: widget.model,
                        location: location,
                      )
              )
          );
        },
        child: Card(
          elevation: 20,
          color: Colors.transparent,
          child: Container(
            padding: EdgeInsets.only(left: 10),
            decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(5),bottomLeft: Radius.circular(5),topRight:Radius.circular(5),bottomRight: Radius.circular(5) )
            ),
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(topRight: Radius.circular(5),bottomRight: Radius.circular(5))
              ),
              padding: EdgeInsets.only(left: 10,right: 10,top: 10,bottom: 7),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Text(widget.model.alertTitle,
                                style: TextStyle(
                                  fontSize: 17,
                                  fontWeight: FontWeight.w600
                                ),
                              ),
                            ),
                            Container(
                              child: Text(widget.model.vehicleNo,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600
                                ),
                              ),
                            )
                          ],
                        ),
                        Container(
                          width: 60,
                          height: 30,
                          child: FlatButton(
                              onPressed: (){},
                              padding: EdgeInsets.only(left: 0),
                              textColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(45))
                              ),
                              color: appsSubColor,
                              child: Text("Ack")
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    child: Text(
                      widget.model.locationAddress
                    ),
                  ),
                  Divider(),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        widget.model.locationAddress.isNotEmpty? Container(
                          child: Image.asset("assets/images/ic_yuma_google_txt.png"),
                        ):Container(),
                        Container(
                          child: Text(
                              DateFormat("yyyy-MM-dd hh:mm:ss a").format(widget.model.timestamp),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
