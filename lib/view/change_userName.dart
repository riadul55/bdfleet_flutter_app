import 'package:bdfleet/extra/loader.dart';
import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/service/apis/profile.dart';
import 'package:bdfleet/service/helper/reponse.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
typedef CallWithTwo(x,y);
class ChangeName extends StatefulWidget {
  final CallWithTwo onChange;
  ChangeName({this.onChange});
  @override
  _ChangeNameState createState() => _ChangeNameState();
}

class _ChangeNameState extends State<ChangeName> {
  TextEditingController firstController = TextEditingController();
  TextEditingController lastController = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    firstController.text = profile.profileInfo.first_name;
    lastController.text = profile.profileInfo.last_name;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 5,bottom: 5),
          child: TextFormField(
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            decoration:  InputDecoration(
              hintText: "First Name",
              labelText: "First Name",
              labelStyle: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
              ),
              hintStyle: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w700,
              ),
              border: OutlineInputBorder(
                  borderSide: BorderSide(
                    width: 2,
                  )),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: appsMainColor,
                ),
              ),
              contentPadding: EdgeInsets.only(left: 15, bottom: 0, top: 0, right: 15),
            ),
            controller: firstController,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 5,bottom: 5),
          child: TextFormField(
            keyboardType: TextInputType.visiblePassword,
            textInputAction: TextInputAction.done,
            decoration:  InputDecoration(
              hintText: "Last Name",
              labelText: "Last Name",
              labelStyle: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w700,
              ),
              hintStyle: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w700
              ),
              border: OutlineInputBorder(
                  borderSide: BorderSide(width: 2)),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: appsMainColor,
                ),
              ),
              contentPadding:
              EdgeInsets.only(left: 15, bottom: 0, top: 0, right: 15),
            ),
            controller: lastController,
          ),
        ),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                textColor: appsMainColor,
                child: Text("Back"),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  widget.onChange(firstController.text,lastController.text);
                },
                textColor: appsMainColor,
                child: Text("Change"),
              ),
              ],
          ),
        )
      ],
    );
  }
}
