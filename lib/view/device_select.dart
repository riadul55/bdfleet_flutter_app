import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/device_model_local.dart';
import 'package:bdfleet/view/change_password.dart';
import 'package:flutter/material.dart';
class DeviceSelect extends StatefulWidget {
  final CallWithOne callWithData;
  final DeviceModelLocal selected;
  DeviceSelect({this.callWithData,this.selected});
  @override
  _DeviceSelectState createState() => _DeviceSelectState();
}

class _DeviceSelectState extends State<DeviceSelect> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Select Vehicle"),
      ),
      body: Container(
        child: ListView(
          children: allDevicesData.map((e){
            bool selected = false;
            if(e.id == widget.selected.id){
              selected = true;
            }
            return Container(
              margin: EdgeInsets.only(top: 10,left: 10,right: 10),
              decoration: BoxDecoration(
                border: Border.all(
                  color: appsSubColor
                )
              ),
              child: FlatButton(
                  onPressed: (){
                    widget.callWithData(e);
                    Navigator.pop(context);
                  },
                  child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                          e.description
                      ),
                      selected?Icon(Icons.check_circle,color: appsSubColor,):Container()
                    ],
                  )
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}
