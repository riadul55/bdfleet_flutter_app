import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/device_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';


class RouteHistoryTextModel extends StatelessWidget {
  final DeviceUpdateModel model;
  RouteHistoryTextModel({this.model});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      child: Column(
        children: [
          Expanded(
            child: Row(
              children: [
                Container(
                    width: 60,
                    color: appsSubColor,
                    alignment: Alignment.center,
                    child: Text(DateFormat('dd\nMMM').format(model.timestamp.toLocal()),
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white
                      ),
                    )
                ),
                Expanded(
                    child: Container(
                      padding: EdgeInsets.only(left: 10,right: 10),
                      child: Text(
                          "Location : \n"
                              "${model.locationName==null?"Loading":model.locationName.replaceAll(", Bangladesh","")}"
                      ,style: TextStyle(
                        color: Colors.black,fontWeight: FontWeight.w400
                      ),
                      ),
                    )
                ),
                Container(
                  color: Colors.black,
                  padding: EdgeInsets.all(5),
                  child: Container(
                      width: 60,
                      decoration: BoxDecoration(
                          color: Colors.black,
                          border:  Border.all(
                              color: Colors.white,
                              style: BorderStyle.solid
                          )
                      ),
                      alignment: Alignment.center,
                      child: Text("${model.speed}\nKm/h",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.white
                        ),
                      )
                  ),
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(top: 2,bottom: 0),
            child: Text(
                DateFormat('hh:mm:ss a').format(model.timestamp.toLocal()),
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.bold
                ),
            ),
          )
        ],
      ),
    );
  }
}
