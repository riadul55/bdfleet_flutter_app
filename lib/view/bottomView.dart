import 'package:bdfleet/extra/loader.dart';
import 'package:bdfleet/extra/theme_Data.dart';
import 'package:bdfleet/model/device_model_local.dart';
import 'package:bdfleet/pages/home/device_alerts.dart';
import 'package:bdfleet/pages/home/navigation.dart';
import 'package:bdfleet/pages/home/playback_location.dart';
import 'package:bdfleet/pages/home/tracking.dart';
import 'package:bdfleet/provider/device_update_provider.dart';
import 'package:bdfleet/service/apis/device_alert.dart';
import 'package:bdfleet/service/helper/reponse.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'package:get/get.dart';

class DashBoardBottomSheet extends StatefulWidget {
  final DeviceModelLocal device;
  DashBoardBottomSheet(this.device);

  @override
  _DashBoardBottomSheetState createState() => _DashBoardBottomSheetState();
}

class _DashBoardBottomSheetState extends State<DashBoardBottomSheet> {
  bool details = true;
  @override
  Widget build(BuildContext context) {
    return Consumer<DeviceUpdate>(
        builder: (ctx,data,_){
          var format = new DateFormat('yyyy-MM-dd hh:mm:ss a');
          double width = Get.width;
          return FittedBox(
            fit: BoxFit.fitWidth,
            alignment: Alignment.center,
            child: Container(
              color: Colors.white,
              width: width,
              margin: EdgeInsets.only(bottom: Get.bottomBarHeight),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            width:(width-40)/2,
                            alignment: Alignment.center,
                            child: Text(
                              "Last active",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black
                              ),
                            )
                        ),
                        Container(
                          height: 25,
                          width: 25,
                          child: IconButton(
                              icon: Icon(details?Icons.keyboard_arrow_down:Icons.keyboard_arrow_up),
                              padding:EdgeInsets.only(left: 0,right: 0,top: 0,bottom: 0),
                              onPressed: (){
                                setState(() {
                                  details =  details? false:true;
                                });
                              }
                          ),
                        ),
                        Container(
                          width:(width-40)/2,
                          alignment: Alignment.center,
                          child: Text("Last Positioning",
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.black
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            width:(width-40)/2,
                            alignment: Alignment.center,
                            child: Text(
                              format.format(widget.device.lastTimestamp.toLocal()),
                              style: TextStyle(
                                  color: Colors.black.withOpacity(.8)
                              ),
                            )
                        ),
                        SizedBox(
                          height: 25,
                          width: 25,
                        ),
                        Container(
                          width:(width-40)/2,
                          alignment: Alignment.center,
                          child: Text( format.format(widget.device.timestamp.toLocal()),
                            style: TextStyle(
                                color: Colors.black.withOpacity(.8)
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    child: Text(widget.device.description,
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Colors.black
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 2,bottom: 2),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                            alignment: Alignment.center,
                            child: Text(
                              widget.device.speed +" km/h \n "+
                                  (widget.device.engine == EngineStatus.off?"OFF":widget.device.runningStatus==RunningStatus.running?"Moving":
                                  widget.device.runningStatus==RunningStatus.slow?"Slow":
                                  widget.device.runningStatus==RunningStatus.over?"Over":"Idle"
                                  ),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.black.withOpacity(.8)
                              ),
                            )
                        ),
                        Container(
                            alignment: Alignment.center,
                            child: Text(
                              "Engine\n "+
                                  (widget.device.engine == EngineStatus.off?"OFF":"ON"
                                  ),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.black.withOpacity(.8)
                              ),
                            )
                        ),
                        widget.device.millage<=0?Container(
                            alignment: Alignment.center,
                            child: Text(
                              "GSM\n (${widget.device.gsmSignal})",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.black.withOpacity(.8)
                              ),
                            )
                        ):Container(
                            alignment: Alignment.center,
                            child: Text(
                              "Millage\n(${widget.device.millage})",
                              style: TextStyle(
                                  color: Colors.black.withOpacity(.8)
                              ),
                              textAlign: TextAlign.center,
                            )
                        ),
                        Container(
                            alignment: Alignment.center,
                            child: Text(
                              "Charge\n "+ widget.device.battery.toString(),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.black.withOpacity(.8)
                              ),
                            )
                        ),
                      ],
                    ),
                  ),
                  Divider(
                    height: 10,
                    color: Colors.black,
                  ),
                  !details?Container():Container(
                      margin: EdgeInsets.only(left: 20,right: 20,top: 0,bottom: 3),
                      height: 30,
                      child: FlatButton(
                        color: appsSubColor,
                        textColor: Colors.white,
                        disabledColor: appsSubColor,
                        disabledTextColor: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)
                        ),
                        onPressed:widget.device.locationName.isEmpty?null: () {
                          setState(() {
                            widget.device.locationName = "";
                            Provider.of<DeviceUpdate>(context,listen: false).updateAll();
                          });
                        },
                        child: Text(widget.device.locationName.isEmpty?"Loading":"Refresh"),
                      )
                  ),
                  !details?Container():Container(
                      margin: EdgeInsets.only(left: 20,right: 20,),
                      child: Text(widget.device.locationName)
                  ),
                  !details?Container():Divider(
                    height: 5,
                    color: Colors.black,
                  ),
                  !details?Container():Container(
                    margin: EdgeInsets.only(top: 2,bottom: 2),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        CustomButton(
                          label: "Tracking",
                          buttonImage: Icon(
                            Icons.location_on,
                            color: appsSubColor,
                          ),
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder: (ctx)=>Tracking(deviceModelLocal: widget.device)));
                          },
                        ),
                        CustomButton(
                          label: "Alerts",
                          buttonImage: Container(
                            height: 25,
                            width: 25,
                            child: Icon(
                              Icons.taxi_alert,
                              color: appsSubColor,
                            ),
                          ),
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder: (ctx)=>DeviceAlerts(deviceModelLocal: widget.device)));
                          },
                        ),
                        CustomButton(
                          label: "Navigation",
                          buttonImage: Container(
                            height: 25,
                            width: 25,
                            child: Icon(
                              Icons.navigation,
                              color: appsSubColor,
                            ),
                          ),
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder: (ctx)=>Navigation(deviceModelLocal: widget.device)));
                          },
                        ),
                        CustomButton(
                          label: "Power",
                          buttonImage: Container(
                            height: 25,
                            width: 25,
                            child: Icon(
                              Icons.power_settings_new_outlined,
                              color: appsSubColor,
                            ),
                          ),
                        ),
                        CustomButton(
                          label: "Play Back",
                          buttonImage: Container(
                            height: 25,
                            width: 25,
                            child: Icon(
                              Icons.alt_route,
                              color: appsSubColor,
                            ),
                          ),
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder: (ctx)=>LocationPlayBack(deviceModelLocal: widget.device)));
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
class CustomButton extends StatelessWidget{
  final String label;
  final buttonImage;
  final VoidCallback onPressed;
  CustomButton({this.label,this.buttonImage,this.onPressed});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: 75,
      height: 45,
      child: FlatButton(
        onPressed: onPressed,
        padding: EdgeInsets.only(left: 0,right: 0,top: 0,bottom: 0),
        child: Column(
          children: [
            Container(
              child: buttonImage,
            ),
            Container(
              child: Text(label),
            )
          ],
        ),
      ),
    );
  }

}

